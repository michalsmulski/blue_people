import UIKit
//import Stamps
//import Shops
//import Web

class MenuViewController: UIViewController, UITableViewDelegate, CouponsAuthDelegate {
	
	private(set) var dataSource: MenuDataSource!
	
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var backgroundImageView: UIImageView!
	
	private func setup() {
		dataSource = MenuDataSource(tableView: tableView)
	}
	
	func selectFirstItem() {
		let indexPath = NSIndexPath(forRow: 0, inSection: 0)
		selectItemIfAvailable(indexPath)
		tableView.selectedIndexPath = indexPath
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.32 * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), {
			self.tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: .None)
		})
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setup()
	}
	
	// MARK: app
	func displayMenu() {
		tableView.reloadData()
	}
	
	func reload() {
		displayMenu()
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		selectItemIfAvailable(indexPath)
	}
	
	func selectItemIfAvailable(indexPath: NSIndexPath) {
		if let item = dataSource[indexPath.row] {
			selectItem(item)
		}
	}
	
	func selectScreen(name name: MenuItems) {
		let items = dataSource.items.filter { $0.key == name}
		if items.count > 0 {
			let firstItem = items[0]
			if let index = dataSource.items.indexOf(firstItem) {
				selectItem(firstItem)
				let indexPath = NSIndexPath(forRow: index, inSection: 0)
				tableView.selectedIndexPath = indexPath
				tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: .None)
			}
		}
	}
	
	private func selectItem(item: MenuItem) {
		var viewController: UIViewController?
		// present view controller
		switch item.key {
		case .Coupons:
			viewController = CouponsMainScreenViewController.create()
            (viewController as! CouponsMainScreenViewController).delegate = self
		default:
			viewController = nil
		}
		if let viewController = viewController {
			presentCenterViewController(viewController)
		}
	}
	
	private func presentAuthenticationView() {
		let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
		delegate.router.presentLoginView(animated: true)
	}
	
	private func presentCenterViewController(viewController: UIViewController) {
		let navigationController = UINavigationController(rootViewController: viewController)
		navigationController.navigationBar.translucent = false
		mm_drawerController.setCenterViewController(navigationController, withCloseAnimation: true, completion: { finished in
			navigationController.navigationBar.topItem!.leftBarButtonItem = self.menuButton()
		})
	}
	
	private func menuButton() -> UIBarButtonItem {
		let button = UIBarButtonItem(image: UIImage(named: "menu_icon")!.imageWithRenderingMode(.AlwaysOriginal), style: .Plain, target: self, action: "menuButtonTapped:")
		button.accessibilityLabel = "menu_button"
		return button
	}
	
	func menuButtonTapped(sender : UIBarButtonItem) {
		mm_drawerController.toggleDrawerSide(.Left, animated: true, completion: nil)
	}
    
    //MARK: CouponsAuthDelegate
    func loginUserForCoupons() {
        self.presentAuthenticationView()
    }
}