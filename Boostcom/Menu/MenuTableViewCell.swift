import UIKit

public class MenuTableViewCell: UITableViewCell {
	
	static let reuseId = "MenuTableViewCell"
	static let preferredHeight: CGFloat = 60.0
	
	@IBOutlet weak var iconImageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	
	public required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
	}
	
	override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
	}
	
	override public func setSelected(selected: Bool, animated: Bool) {
		bm_setSelected(selected, animated: animated)
	}
	
	override public func setHighlighted(highlighted: Bool, animated: Bool) {
		bm_setSelected(highlighted, animated: animated)
	}
	
	private func bm_setSelected(selected: Bool, animated: Bool) {
		let selectionBlock: Void -> Void = {
			if selected {
			self.backgroundColor = UIColor(hex: 0xefeded)
		} else {
			self.backgroundColor = UIColor.whiteColor()
			}
			self.iconImageView.highlighted = selected
		}
		
		if animated {
			UIView.transitionWithView(self.contentView, duration: 0.32, options: .CurveEaseInOut, animations: {
				selectionBlock()
			}, completion: nil)
		} else {
			selectionBlock()
		}
	}
}
