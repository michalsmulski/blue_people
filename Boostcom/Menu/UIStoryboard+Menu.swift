import UIKit

extension UIStoryboard {
    
    static func menu() -> UIStoryboard {
        return UIStoryboard(name: "Menu", bundle: nil)
    }
    
    func menuViewController() -> MenuViewController {
        return instantiateViewControllerWithIdentifier("MenuViewController") as! MenuViewController
    }
}
