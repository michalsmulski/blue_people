import UIKit

enum MenuItems {
	case Coupons
	case GiftCard
	case Inspiration
	case Shops
	case Receipts
	case Profile
	case MemberCard
	case HomePage
	case Authentication
}

public class MenuItem: Hashable, Equatable {
	
	private(set) var id: Int
    private(set) var icon: UIImage
    private(set) var selectedIcon: UIImage?
    private(set) var displayTitle: String
    private(set) var key: MenuItems
	
	public var hashValue: Int {
		return id
	}
    
    init(icon: UIImage, selectedIcon: UIImage? = nil, displayTitle: String, key: MenuItems) {
		self.id = Int(arc4random() % 1000)
        self.icon = icon
        self.selectedIcon = selectedIcon
        self.displayTitle = displayTitle
        self.key = key
    }
}

public func ==(lhs: MenuItem, rhs: MenuItem) -> Bool {
	return lhs.hashValue == rhs.hashValue
}