import Foundation

class MenuAccessFilter {

	private let item: MenuItems

	init(item: MenuItems) {
		self.item = item
	}

	func execute() -> Bool {
		return MenuAccessFilter.accessTable().contains(item) && !UserSession.sharedSession.isOpen()
	}

	private static func accessTable() -> [MenuItems] {
		return [
			MenuItems.GiftCard,
			MenuItems.Receipts,
            MenuItems.MemberCard
		]
	}
}
