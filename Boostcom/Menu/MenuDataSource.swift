import UIKit
//import Identity

class MenuDataSource: NSObject, UITableViewDataSource {
	
	private(set) var items: [MenuItem] = []
	private(set) weak var tableView: UITableView!
	
	init(tableView: UITableView) {
		self.tableView = tableView
		super.init()
		
		setupItems()
		self.tableView.dataSource = self
		self.tableView.rowHeight = MenuTableViewCell.preferredHeight
	}
	
	private func setupItems() {
		let items = [
			MenuItem(icon: UIImage(named: "menu_offers")!, displayTitle: "Customer club".uppercaseString, key: .Coupons)
		]
		
		self.items = items
	}
	
	subscript(index: Int) -> MenuItem? {
		return items[index]
	}
	
	// MARK: tableView
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return items.count
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier(MenuTableViewCell.reuseId, forIndexPath: indexPath) as! MenuTableViewCell
		populate(cell, atIndexPath: indexPath)
		return cell
	}
	
	private func populate(cell: MenuTableViewCell, atIndexPath indexPath: NSIndexPath) {
		let item = items[indexPath.row]
		cell.titleLabel.text = item.displayTitle
		cell.iconImageView.image = item.icon
		cell.iconImageView.highlightedImage = item.selectedIcon
	}
}
