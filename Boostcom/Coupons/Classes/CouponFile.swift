//
//  CouponFile.swift
//  Boostcom
//
//  Created by Zmicier Biesau on 22.1.16.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import Foundation
import SwiftyJSON


public class CouponFile {
    
    public var lastModified: NSNumber = 0
    public var thumbnailWidth: Float = 0
    public var couponId: String = ""
    public var thumbnailUrl: String = ""
    public var originalHeight: Float = 0
    public var thumbnailHeight: Float = 0
    public var originalWidth: Float = 0
    public var originalUrl: String = ""
    public var couponFileId: String = ""
    
    public init() {}
    
    public init(json: JSON) {
        parse(json)
    }
    
    private func parse(json: JSON) {
        self.lastModified = json["lastModified"].number!
        self.thumbnailWidth = Float(json["thumbnailWidth"].string!)!
        self.couponId = json["offerId"].string!
        self.thumbnailUrl = json["thumbnailUrl"].string!
        self.originalHeight = Float(json["originalHeight"].string!)!
        self.thumbnailHeight = Float(json["thumbnailHeight"].string!)!
        self.originalWidth = Float(json["originalWidth"].string!)!
        self.originalUrl = json["originalUrl"].string!
        self.couponFileId = json["offerFileId"].string!
    }
}