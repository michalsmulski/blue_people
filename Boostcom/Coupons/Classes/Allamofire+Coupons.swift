//
//  Allamofire+Coupons.swift
//  Boostcom
//
//  Created by Zmicier Biesau on 21.1.16.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import UIKit

import Alamofire

extension NSMutableURLRequest {
    func updateHTTPHeader(APIKey APIKey: String) {
        setValue(APIKey, forHTTPHeaderField: "apiKey")
        
    }
    
    func updateHTTPHeader(APIKey APIKey: String, phoneNumber: String, countryCode: String) {
    setValue(APIKey, forHTTPHeaderField: "apiKey")
        setValue(phoneNumber, forHTTPHeaderField: "mobileNumber")
        setValue(countryCode, forHTTPHeaderField: "countryCode")
    }
}

class APICouponRequest: URLRequestConvertible {
    
    private var method: Alamofire.Method
    private var URL: NSURL
    private var parameters: [String: AnyObject]?
    
    
    
    init(method: Alamofire.Method, URL: NSURL,  parameters: [String: AnyObject]? = nil) {
        
        self.method = method
        self.URL = URL
        self.parameters = parameters
        
    }
    
    var URLRequest: NSMutableURLRequest {
        
        let request = NSMutableURLRequest(URL: URL)
        
        request.HTTPMethod = method.rawValue
        
        let encoding = ParameterEncoding.URL
        return encoding.encode(request, parameters: parameters).0
    }
}
