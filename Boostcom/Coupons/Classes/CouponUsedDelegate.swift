//
//  CouponUsedDelegate.swift
//  Boostcom
//
//  Created by Zmicier Biesau on 2.2.16.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import Foundation

protocol CouponUseDelegate:class {
    func cuponUsedWithId(couponId: String)
    
}