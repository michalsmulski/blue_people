//
//  CouponsConst.swift
//  Boostcom
//
//  Created by Zmicier Biesau on 1.2.16.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import UIKit

struct CouponsConst {
    struct ResponseData
    {
        static let kActivatedOfferMessage = "OFFER_ALREADY_ACTIVE"
    }
    struct Details {
        static let kDeltaUseButtonAnimationVerticalOffset: CGFloat = 10
        static let kUseButtonHorizontalOffset: CGFloat = 25
        static let kUseButtonAnimationHorizontalOffset: CGFloat = 10
        static let kDefaultCouponTime: NSTimeInterval = 30
        static let kContainerUseButtonTag = 1000
    }
}