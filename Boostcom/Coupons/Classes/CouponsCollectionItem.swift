//
//  CouponCollectionItem.swift
//  Boostcom
//
//  Created by Zmicier Biesau on 22.1.16.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import Foundation
import SwiftyJSON

public class CouponsCollectionItem
{
    public var collectionId: String = ""
    public var collectionName: String? = ""
    private(set) var coupons: [CouponItem] = []
    
    public init() {}
    
    public init(json: JSON) {
        parse(json)
    }
    
    private func parse(json: JSON) {
        self.collectionId = json["collectionId"].stringValue
        if let name = json["name"].string {
            self.collectionName = name
        }
        if let coupons = json["offers"].array
        {
            for (_, value) in coupons.enumerate() {
                let couponItem = CouponItem.init(json: value)
                self.coupons.append(couponItem)
            }
        }
    }
}