//
//  CouponsAuthDelegate.swift
//  Boostcom
//
//  Created by Zmicier Biesau on 1.2.16.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import Foundation

protocol CouponsAuthDelegate:class {
    func loginUserForCoupons()
    
}
