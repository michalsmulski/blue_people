//
//  CouponDetailsViewController.swift
//  Boostcom
//
//  Created by Zmicier Biesau on 26.1.16.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import UIKit
import Alamofire
import PureLayout
import BFPaperButton
import EasyAnimation
import KVNProgress
import MZTimerLabel

public class CouponDetailsViewController: UIViewController {

    @IBOutlet weak var messageLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var discountImage: UIImageView!
    @IBOutlet weak var aspectOfImage: NSLayoutConstraint!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var useButton: BFPaperButton!
    @IBOutlet weak var leadingUseButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingUseButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightUseButtonConstraint: NSLayoutConstraint!
    
    var couponItem: CouponItem?
    var index: Int?
    var couponResponseData: CouponResponseData?
    var isUsed = false
    var isAnimatingNow = false
    weak var coupondDelegate:CouponUseDelegate?
    
    public var module: AbstractCouponsModule!
    
    static func create() -> CouponDetailsViewController {
        let controller = UIStoryboard(name: "Coupons", bundle: nil).instantiateViewControllerWithIdentifier("CouponDetailsViewController") as! CouponDetailsViewController
        return controller
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.module = CouponsModule()
        
        self.bottomView.layer.borderWidth = 0.5
        self.bottomView.layer.borderColor = UIColor(hex: 0xE5E5E5).CGColor
        
        self.setupUseButton()
        self.defineMessage()
        self.resetImageConstraint()
        self.imageLoad()
        if !self.isCouponActivated()
        {
            if !self.isCouponAvailable()
            {
                self.markUsed()
            }
        }
        
    }
    
    public override func viewWillAppear(animated: Bool) {
        if !self.isAnimatingNow && self.isCouponActivated()
        {
            self.couponResponseData = CouponResponseData(couponId: self.couponItem!.couponId)
            self.increaseButton(couponResponse: self.couponResponseData!)
        }
    }
    private func defineMessage()
    {
        if couponItem!.limitedUse
        {
            let usesLeft = couponItem!.limitedTo - couponItem!.numberOfTimesUsedByUser
            if usesLeft > 0
            {
                self.messageLabel.text = "You can use this coupon \(usesLeft) time(s)"
            }
            else
            {
                self.messageLabel.text = "This coupon can't be reused"
                self.useButton.enabled = false
            }
        }
        else
        {
            self.messageLabel.text = "You can use this coupon as many times as you want"
        }
    }
    
    private func resetImageConstraint()
    {
        self.discountImage.removeConstraint(self.aspectOfImage)
        let multiplier = CGFloat(self.couponItem!.couponFile.originalHeight) / CGFloat(self.couponItem!.couponFile.originalWidth)
        self.discountImage.autoMatchDimension(.Height, toDimension: .Width, ofView: self.discountImage, withMultiplier: multiplier)
    }
    
    private func imageLoad()
    {
        let imageURL = NSURL(string: couponItem!.couponFile.originalUrl)
        Alamofire.request(.GET, imageURL!).response() {
            (_, _, data, _) in
            
            let image = UIImage(data: data!)
            self.discountImage.image = self.isUsed ? image?.toGrayscale() : image
        }
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isCouponActivated() ->Bool
    {
        if let activationDate = CouponsActivationInfo.sharedInstance.getActivationTime(couponId: self.couponItem!.couponId)
        {
            let countDown = self.getCountDownTime(activationDate)
            if countDown > 1 // no sense to show animation for 1 sec displaying, it may take longer to show it
            {
                
                return true
            }
        }
        return false
    }
    
    private func isCouponAvailable() -> Bool
    {
        
        if !self.couponItem!.limitedUse || (self.couponItem!.limitedTo > self.couponItem!.numberOfTimesUsedByUser)
        {
            self.isUsed = false
            return true
        }
        else
        {
            self.isUsed = true
            return false
        }
    }
    
    private func markUsed()
    {
        self.useButton.hidden = true
        self.defineMessage()
        self.discountImage.image?.toGrayscale()
        
        let useMarkImage = UIImageView(image: UIImage(named: "used_offer_ribon"))
        useMarkImage.frame = CGRectMake(self.view.bounds.width, self.view.bounds.height, useMarkImage.bounds.width, useMarkImage.bounds.height)
        self.discountImage.addSubview(useMarkImage)
        useMarkImage.autoPinEdgeToSuperviewEdge(.Top)
        useMarkImage.autoPinEdgeToSuperviewEdge(.Right)
        useMarkImage.autoMatchDimension(.Width, toDimension: .Width, ofView: self.discountImage, withMultiplier: 3 / 7)
        useMarkImage.autoMatchDimension(.Width, toDimension: .Height, ofView: useMarkImage)
        self.messageLeadingConstraint.constant = -61
        
        self.isAnimatingNow = false
        self.coupondDelegate?.cuponUsedWithId(self.couponItem!.couponId)
    }
    
//MARK:button USE displaying logic
    
    private func setupUseButton()
    {
        self.useButton.shadowColor = UIColor.darkGrayColor()
        self.useButton.backgroundColor = UIColor(hex: 0x228B22)
        self.useButton.cornerRadius = self.useButton.bounds.size.height / 2
    }
    
    @IBAction func buttonClick(sender: AnyObject) {
        self.showUseQuestion()
        
    }
    
    func getCountDownTime(activationTime:NSDate) -> NSTimeInterval
    {
        let activationTimeInterval = activationTime.timeIntervalSince1970
        let nowTimeInterval = NSDate().timeIntervalSince1970
        
        let countDownTime = CouponsConst.Details.kDefaultCouponTime - (nowTimeInterval - activationTimeInterval)
        return countDownTime
    }
    
    private func showUseQuestion()
    {
        let controller = UIAlertController(
            title: UIApplication.sharedApplication().applicationName(),
            message: "You are about to use the coupon. It will be active for \(Int(CouponsConst.Details.kDefaultCouponTime)) seconds. Present coupon to staff",
            preferredStyle: .Alert)
        let cancelAction = UIAlertAction(title: "No", style: .Cancel, handler: nil)
        let loginAction = UIAlertAction(title: "Yes", style: .Default, handler: { action in
            self.requestCouponUse()
            KVNProgress.showWithStatus("Loading")
        })
        controller.addAction(cancelAction)
        controller.addAction(loginAction)
        self.presentViewController(controller, animated: true, completion: nil)
    }
    
   
    //MARK: Animating button USE
    
    private func increaseButton(couponResponse couponResponse:CouponResponseData)
    {
        self.isAnimatingNow = true
        let countDownTime = self.getCountDownTime(couponResponse.activationTime)
        
        if countDownTime < 1
        {
            self.showErrorGettingCoupon()
            return
        }
     
        self.useButton.setTitle("", forState: .Normal)
        self.messageLabel.text = ""
        self.useButton.shadowColor = UIColor.clearColor()
        self.useButton.removeTarget(self, action: nil, forControlEvents: .TouchUpInside)
        self.useButton.clipsToBounds = true
        
        let offset: CGFloat = ((self.view.bounds.width - 150) * (1 - CGFloat(countDownTime / CouponsConst.Details.kDefaultCouponTime))) / 2
                                        + CouponsConst.Details.kUseButtonAnimationHorizontalOffset// w(1 - k) / 2 + 10
        self.leadingUseButtonConstraint.constant = offset
        self.trailingUseButtonConstraint.constant = offset
        self.leadingUseButtonConstraint.priority = 999
        self.heightUseButtonConstraint.constant += CouponsConst.Details.kDeltaUseButtonAnimationVerticalOffset
        
        
        
        UIView.animateWithDuration(0.5, animations: {
            self.view.layoutIfNeeded()
            },
            completion: {(complete: Bool) in
                self.decreaseButton(countDownTime: countDownTime)
        })
        
    }
    
    private func addTextWhileDecreasing(countDownTime countDownTime:NSTimeInterval)
    {
        let containerView = UIView(frame: self.useButton.bounds)
        self.useButton.addSubview(containerView)
        containerView.autoCenterInSuperview()
        containerView.autoSetDimensionsToSize(containerView.bounds.size)
        containerView.tag = CouponsConst.Details.kContainerUseButtonTag
        
        
        let label = UILabel(frame: CGRectMake(0, 15, self.useButton.bounds.width, 20))
        label.text = "Coupon expires:"
        label.font = UIFont.systemFontOfSize(18.0)
        label.textAlignment = .Center
        label.textColor = UIColor.whiteColor()
        containerView.addSubview(label)
        
        let countDown = UILabel(frame: CGRectMake(0, 40, self.useButton.bounds.width, 20))
        countDown.font = UIFont.systemFontOfSize(18.0)
        countDown.textAlignment = .Center
        countDown.textColor = UIColor.whiteColor()
        containerView.addSubview(countDown)
        
        let timer = MZTimerLabel(label: countDown, andTimerType: MZTimerLabelTypeTimer)
        timer.timeFormat = "mm:ss"
        
        timer.setCountDownTime(countDownTime)
        timer.start()
        
    }
    
    private func decreaseButton(countDownTime countDownTime:NSTimeInterval)
    {
        self.addTextWhileDecreasing(countDownTime: countDownTime)
        self.useButton.shadowColor = UIColor.clearColor()
        self.leadingUseButtonConstraint.constant = self.view.bounds.width / 2 - 75
        self.trailingUseButtonConstraint.constant = self.view.bounds.width / 2 - 75
        
        UIView.animateWithDuration(countDownTime,  animations: {
            self.view.layoutIfNeeded()
        },
            completion: { (complete: Bool) in
                self.heightUseButtonConstraint.constant -= CouponsConst.Details.kDeltaUseButtonAnimationVerticalOffset
                self.leadingUseButtonConstraint.priority = 1
                self.trailingUseButtonConstraint.constant = CouponsConst.Details.kUseButtonHorizontalOffset
                self.useButton.viewWithTag(CouponsConst.Details.kContainerUseButtonTag)?.removeFromSuperview()
                self.useButton.setTitle("Use".uppercaseString, forState: .Normal)
                self.defineMessage()
                self.useButton.shadowColor = UIColor.darkGrayColor()
                self.useButton.addTarget(self, action: "buttonClick:", forControlEvents: .TouchUpInside)
                if !self.isCouponAvailable()
                {
                    self.markUsed()
                }
                self.putButtonsBack()
        })
    }
    
    private func putButtonsBack()
    {
        UIView.animateWithDuration(0.5, animations: {
                self.view.layoutIfNeeded()
        })
    }
 
    //MARK:requesting coupon
    private func requestCouponUse()
    {
        
        module.APIClient.requestCouponUse(
            couponId:(self.couponItem?.couponId)!,
            completion:
            { [unowned self] couponResponse in
            if couponResponse != nil {
                self.couponResponseData = couponResponse
                self.couponResponseData!.connectActivationTimeWithId(self.couponItem!.couponId)
                KVNProgress.dismiss()
                self.couponItem?.numberOfTimesUsedByUser++
                self.increaseButton(couponResponse: self.couponResponseData!)
            }
            
            }, failure: { [unowned self] error in
                ErrorController(parentViewController: self).handle(error, retryBlock: { [unowned self] in
                    self.requestCouponUse()
                    })
            })
        
    }
    
    private func showErrorGettingCoupon()
    {
        let controller = UIAlertController(
            title: UIApplication.sharedApplication().applicationName(),
            message: "Error while getting coupon",
            preferredStyle: .Alert)
        let okAction = UIAlertAction(title: "Ok", style: .Cancel, handler: nil)
        controller.addAction(okAction)

        self.presentViewController(controller, animated: true, completion: nil)
    }

    
}
