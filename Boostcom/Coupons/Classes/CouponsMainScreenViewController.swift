//
//  TestViewController.swift
//  Boostcom
//
//  Created by Michał Smulski on 11/01/16.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import UIKit
import DKTabPageViewController
import KVNProgress
import PureLayout

public class CouponsMainScreenViewController: UIViewController, CouponsAuthDelegate{
    
    var items = NSMutableArray(capacity: 2)
    var couponsSource : CouponsDataSource?
    var module: AbstractCouponsModule!
    weak var delegate:CouponsAuthDelegate?
    
    static func create() -> CouponsMainScreenViewController {
        let viewController = CouponsMainScreenViewController()
        return viewController
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.navigationBar.tintColor = UIColor.blackColor()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
        
        self.title = "List of discounts"
        self.module = CouponsModule()
        KVNProgress.showWithStatus("Loading")
        requestCoupons()
        
    }
    
    private func fillControllers()
    {
        for var index = 0; index < (self.couponsSource?.items.count)!; index++
        {
            let vc = CouponsListViewController.create((self.couponsSource?.items[index])!)
            let item = DKTabPageViewControllerItem.tabPageItemWithTitle(self.couponsSource?.items[index].collectionName!.uppercaseString, viewController: vc)
            items.addObject(item)
        }
//        TODO: 2 vc just to show how tabbar is working
//        let vc1 = CouponsListViewController.create((self.couponsSource?.items[0])!)
//        vc1.delegate = self
//        let item1 = DKTabPageViewControllerItem.tabPageItemWithTitle(self.couponsSource?.items[0].collectionName!.uppercaseString, viewController: vc1)
//        items.addObject(item1)
//        
//        let vc2 = CouponsListViewController.create((self.couponsSource?.items[0])!)
//        vc2.delegate = self
//        let item2 = DKTabPageViewControllerItem.tabPageItemWithTitle(self.couponsSource?.items[0].collectionName!.uppercaseString, viewController: vc2)
//        items.addObject(item2)
        ////////////////////////
        
        let tabPageViewController = DKTabPageViewController(items: items as [AnyObject])
        tabPageViewController.tabPageBar.titleColor = UIColor.darkGrayColor()
        tabPageViewController.tabPageBar.selectedTitleColor = UIColor.blackColor()
        tabPageViewController.tabPageBar.selectedIndicatorColor = UIColor.blackColor()
        
        self.addChildViewController(tabPageViewController)
        self.view.addSubview(tabPageViewController.view)
        tabPageViewController.view.autoPinEdgesToSuperviewEdges()
    }
    
    private func requestCoupons()
    {
        module.APIClient.requestCouponsList({ [unowned self] couponsDataSource in
            if couponsDataSource != nil {
                self.couponsSource = couponsDataSource
                if self.couponsSource != nil
                {
                    self.fillControllers()
                    KVNProgress.dismiss()
                }
            }
            
            }, failure: { [unowned self] error in
                ErrorController(parentViewController: self).handle(error, retryBlock: { [unowned self] in
                    self.requestCoupons()
                    })
            })
        
    }
    
    //MARK: CouponsAuthDelegate
    func loginUserForCoupons() {
        delegate?.loginUserForCoupons()
    }
}