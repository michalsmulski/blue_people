//
//  CouponsActivationInfo.swift
//  Boostcom
//
//  Created by Zmicier Biesau on 1.2.16.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import Foundation

public class CouponsActivationInfo {
    public static let sharedInstance:CouponsActivationInfo = CouponsActivationInfo()
    //private init
    private init(){}
    
    private var couponsDictionary: NSMutableDictionary = [:]
    
    func setActivationTime(time time: NSDate, couponId: String)
    {
        self.couponsDictionary[couponId] = time
    }
    
    func getActivationTime(couponId couponId: String) -> NSDate?
    {
        return self.couponsDictionary[couponId] as? NSDate
    }
}
