//
//  CouponsList1ViewController.swift
//  Boostcom
//
//  Created by Zmicier Biesau on 21.1.16.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import UIKit

private enum AuthenticationProcessState: Int {
    case Unknown = 0
    case CouponsFound = 1
}

public class CouponsListViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, CouponUseDelegate {
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    var couponsCollectionItem : CouponsCollectionItem?
    weak var delegate:CouponsAuthDelegate?
    
    static func create(couponsCollectionItem: CouponsCollectionItem) -> CouponsListViewController {
        let controller = UIStoryboard(name: "Coupons", bundle: nil).instantiateViewControllerWithIdentifier("CouponsListViewController") as! CouponsListViewController
        controller.couponsCollectionItem = couponsCollectionItem
        return controller
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func showActiveCouponsPages(animated animated: Bool, couponNumber: Int)
    {
        if self.isCouponActive(couponNumber: couponNumber)
        {
            let activeCoupons = self.activeCoupons()
            let couponspagesController = CouponsPagesViewController.create(activeCoupons, startCouponId: self.couponsCollectionItem!.coupons[couponNumber].couponId)
            couponspagesController.title = self.couponsCollectionItem?.collectionName
            couponspagesController.coupondDelegate = self
            
            self.navigationController?.pushViewController(couponspagesController, animated: animated)
        }
    }
    
    private func activeCoupons() -> [CouponItem]
    {
        var activeCoupons:[CouponItem] = []
        for (idx, value) in self.couponsCollectionItem!.coupons.enumerate() {
            if self.isCouponActive(couponNumber: idx)
            {
                activeCoupons.append(value)
            }
        }
        return activeCoupons
    }
    private func isCouponActive(couponNumber couponNumber: Int) -> Bool
    {
        let coupon = self.couponsCollectionItem!.coupons[couponNumber]
        if !coupon.limitedUse || (coupon.limitedTo > coupon.numberOfTimesUsedByUser)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    private func showLoginQuestion()
    {
        let controller = UIAlertController(
            title: UIApplication.sharedApplication().applicationName(),
            message: "To access this you must be logged in. Want to login?",
            preferredStyle: .Alert)
        let cancelAction = UIAlertAction(title: "No", style: .Cancel, handler: nil)
        let loginAction = UIAlertAction(title: "Yes", style: .Default, handler: { action in
            self.delegate?.loginUserForCoupons()
        })
        controller.addAction(cancelAction)
        controller.addAction(loginAction)
        self.presentViewController(controller, animated: true, completion: nil)
    }
    
    
    // MARK: CollectionViewDataSource
    public func collectionView(collectionView: UICollectionView,
        numberOfItemsInSection section: Int) -> Int
    {
        return (self.couponsCollectionItem?.coupons.count)!
    }
    
    public func collectionView(collectionView: UICollectionView,
        cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CouponsItemCell", forIndexPath: indexPath) as! CouponsItemCell
        !(self.isCouponActive(couponNumber: indexPath.item)) ? cell.markAsUsed(true) : cell.markAsUsed(false)
        cell.couponFile = self.couponsCollectionItem?.coupons[indexPath.item].couponFile
        

        
        return cell
    }
    
    public func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        let dice1 = self.view.frame.size.width / 2 - 20.0
        let proportion = (self.couponsCollectionItem?.coupons[indexPath.item].couponFile.thumbnailHeight)! / (self.couponsCollectionItem?.coupons[indexPath.item].couponFile.thumbnailWidth)!
        let dice2 = CGFloat(proportion) * dice1
        return CGSizeMake(CGFloat(dice1), CGFloat(dice2))
    }
    
    //MARK: CollectionViewDelegate
    public func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        
        if UserSession.sharedSession.isOpen()
        {
            self.showActiveCouponsPages(animated: true, couponNumber: indexPath.item)
        }
        else
        {
            self.showLoginQuestion()
        }
    
    }

    //MARK: Used coupons delegate
    func cuponUsedWithId(couponId: String)
    {
        let itemNumber = self.couponsCollectionItem!.coupons.indexOf( { $0.couponId == couponId } )!
        if !self.isCouponActive(couponNumber: itemNumber)
        {
            self.collectionView.reloadData()
        }
        
    }

}
