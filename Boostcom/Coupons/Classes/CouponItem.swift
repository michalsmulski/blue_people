//
//  CouponItem.swift
//  Boostcom
//
//  Created by Zmicier Biesau on 22.1.16.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import Foundation
import SwiftyJSON


public class CouponItem {
    
    public var couponId: String = ""
    public var startDateTime: NSDate?
    public var endDateTime: NSDate?
    public var sortPriority: NSInteger = 0
    public var couponHeader: NSString?
    public var storeName: NSString?
    public var couponInfo: NSString?
    public var limitedUse: Bool = false
    public var limitedTo: Int = 0
    public var couponFile: CouponFile = CouponFile()
    public var activatedUntil: NSDate?
    public var numberOfTimesUsedByUser: NSInteger = 0
    public var numberOfTotalLikes: NSInteger = 0
    public var likedByUser: Bool = false
    public var storeID: Int? = 0
    public var couponName: NSString = ""
    
    public init() {}
    
    public init(json: JSON) {
        parse(json)
    }
    
    private func parse(json: JSON) {
        
        self.storeID = Int(json["storeId"].string!)
        if let endDateTime = json["endDatetime"].string {
            self.endDateTime = NSDate(string: endDateTime, format: .yMdFormat)
        }
        if let startDateTime = json["startDatetime"].string {
            self.startDateTime = NSDate(string: startDateTime, format: .yMdFormat)
        }
        if let couponHeader = json["offerHeader"].string {
            self.couponHeader = couponHeader
        }
        self.couponName = json["offerName"].string!
        if let limitedUseStr = json["limitedUse"].string {
            if limitedUseStr == "YES"
            {self.limitedUse = true}
        }
        if let sortPriority = json["sortPriority"].number {
            self.sortPriority = sortPriority.integerValue
        }
        if let numberOfTimesUsedByUser = json["numberOfTimesUsedByUser"].number {
            self.numberOfTimesUsedByUser = numberOfTimesUsedByUser.integerValue
        }
        self.couponId = json["offerId"].string!
        if let activatedUntil = json["activatedUntil"].string {
            self.activatedUntil = NSDate(string: activatedUntil, format: .yMdFormat)
        }
        
        if let couponInfo = json["offerInfo"].string {
            self.couponInfo = couponInfo
        }
        
        if let storeName = json["storeName"].string {
            self.storeName = storeName
        }
        
        if let numberOfTotalLikes = json["numberOfTotalLikes"].number {
            self.numberOfTotalLikes = numberOfTotalLikes.integerValue
        }
        
        if let likedByUser = json["likedByUser"].bool {
            self.likedByUser = likedByUser
        }
        
        if let limitedToStr = json["limitedTo"].string {
            self.limitedTo = Int(limitedToStr)!
        }
        
        self.couponFile = CouponFile.init(json: json["offerFile"])
         
    }

}