//
//  CouponsAPIClient.swift
//  Boostcom
//
//  Created by Zmicier Biesau on 20.1.16.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import Foundation
import Alamofire

public class CouponsAPIClient{
    

    private let baseURL: NSURL
    private let manager = Manager.rejectCookiesManager()

    private let APIKey: String
    
    private let phoneNumber: String?
    private let countryCode: String?
    
    
    public init(baseURL: NSURL, APIKey: String, phoneNumber: String, countryCode: String) {
        self.baseURL = baseURL
        self.phoneNumber = phoneNumber
        self.countryCode = countryCode
        self.APIKey = APIKey
    }
    
    private func baseURLString(username: String) -> String {
        return baseURL.absoluteString + "/" + username
    }
    
    private func extendedBaseURLString(username: String, RESTResourceName: String) -> String {
        return baseURLString(username) + "/" + RESTResourceName
    }
    
    
    public func requestCouponsList(completion: CouponsDataSource? -> Void, failure: NSError -> Void) {
        let URL = NSURL(string: baseURLString("getactive"))!
        let request = APICouponRequest(method: .POST, URL: URL, parameters: self.createListJSON())
        
        
        manager.request(request).response { request, response, data, error in
            if let error = error {
                failure(error)
            } else {
                var couponsDataSource: CouponsDataSource?
                APIResponseController().handle(response, success: {
                    if let data = data {
                        couponsDataSource = CouponsDataSource.init(data: data)
                        completion(couponsDataSource)
                    }
                    }, failure: { error, statusCode in
                        if statusCode == 404 {
                            completion(couponsDataSource)
                        } else {
                            failure(error)
                        }
                })
            }
        }
    }
    
    public func requestCouponUse(couponId couponId: String, completion: CouponResponseData? -> Void, failure: NSError -> Void) {
        let URL = NSURL(string: baseURLString("useoffer"))!
        let request = APICouponRequest(method: .POST, URL: URL, parameters: self.createCouponUseJSON(couponId))
        
        
        manager.request(request).response { request, response, data, error in
            if let error = error {
                failure(error)
            } else {
                var couponsResponseData: CouponResponseData?
                APIResponseController().handle(response, success: {
                    if let data = data {
                        couponsResponseData = CouponResponseData(data: data)
                        completion(couponsResponseData)
                    }
                    }, failure: { error, statusCode in
                        if statusCode == 404 {
                            completion(couponsResponseData)
                        } else {
                            failure(error)
                        }
                })
            }
        }
    }
    
    private func createListJSON() -> [String: AnyObject]
    {
        var dict: [String: AnyObject] = [
            "apiKey": self.APIKey,
            
        ]
        
        if let phoneNumber = phoneNumber {
            dict["mobileNumber"] = phoneNumber
        }
        
        if let countryCode = countryCode {
            dict["countryCode"] = countryCode
        }
        
       return dict
    }
    
    private func createCouponUseJSON(couponId: String) -> [String: AnyObject]
    {
        var dict: [String: AnyObject] = [
            "apiKey": self.APIKey,
            
        ]
        
        if let phoneNumber = phoneNumber {
            dict["mobileNumber"] = phoneNumber
        }
        
        if let countryCode = countryCode {
            dict["countryCode"] = countryCode
        }
        
        dict["offerId"] = couponId
        
        return dict
    }
        
    
}
