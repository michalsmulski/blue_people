//
//  AbstractCouponsModule.swift
//  Boostcom
//
//  Created by Zmicier Biesau on 20.1.16.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import Foundation

public protocol AbstractCouponsModule {
    var APIClient: CouponsAPIClient { get }
}
