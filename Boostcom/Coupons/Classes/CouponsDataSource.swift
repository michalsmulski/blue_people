//
//  CouponsDataSource.swift
//  Boostcom
//
//  Created by Zmicier Biesau on 22.1.16.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import UIKit
import SwiftyJSON

public class CouponsDataSource: NSObject {

    private(set) var items: [CouponsCollectionItem] = []
    
    public init(data: NSData) {
        super.init()
        parse(JSON(data: data))
    }
    
    private func parse(json: JSON) {
        let collections = json["collections"].array
        for (_, value) in collections!.enumerate() {
            let collectionsItem = CouponsCollectionItem.init(json: value)
            items.append(collectionsItem)
        }
        
        
        
    }
    
}
