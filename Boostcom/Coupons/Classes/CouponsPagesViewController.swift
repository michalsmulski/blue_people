//
//  CouponsPagesViewController.swift
//  Boostcom
//
//  Created by Zmicier Biesau on 27.1.16.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import UIKit

class CouponsPagesViewController: UIPageViewController, UINavigationControllerDelegate, UIPageViewControllerDataSource, UIPageViewControllerDelegate, CouponUseDelegate {

    weak var numberLabel: UILabel?
    var coupons: [CouponItem] = []
    var startNumber: Int = 0
    weak var coupondDelegate:CouponUseDelegate?
    
    static func create(coupons: [CouponItem], startCouponId: String) -> CouponsPagesViewController {
        let controller = UIStoryboard(name: "Coupons", bundle: nil).instantiateViewControllerWithIdentifier("CouponsPagesViewController") as! CouponsPagesViewController
        controller.coupons = coupons
        controller.startNumber = coupons.indexOf( { $0.couponId == startCouponId } )!
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        self.dataSource = self
        
        self.view.backgroundColor = UIColor(hex: 0xf7f7f7)

        let initialViewController = self.viewControllerAtIndex(index: startNumber);
        initialViewController.couponItem = self.coupons[startNumber]
        let viewControllers = [initialViewController]
        self.setViewControllers(viewControllers, direction: .Forward, animated: false, completion: nil)
        
        self.displayCurrentPageNumber(currentIndex: startNumber + 1)
        
        // Do any additional setup after loading the view.
    }

    private func displayCurrentPageNumber(currentIndex currentIndex: Int)
    {
        if self.numberLabel == nil
        {
            let titleView = UIView(frame: CGRectMake(0, 0, 200, 44))
            
            let titleLabel = UILabel(frame: CGRectMake(0, 5, 200, 20))
            titleLabel.text = self.title
            titleLabel.font = UIFont.systemFontOfSize(17)
            titleLabel.adjustsFontSizeToFitWidth = true
            titleLabel.textAlignment = .Center
            titleView.addSubview(titleLabel)
            
            let subtitleLabel = UILabel(frame: CGRectMake(0, 25, 200, 20))
            subtitleLabel.font = UIFont.systemFontOfSize(17)
            subtitleLabel.adjustsFontSizeToFitWidth = true
            subtitleLabel.textAlignment = .Center
            titleView.addSubview(subtitleLabel)
            self.navigationItem.titleView = titleView
            self.numberLabel = subtitleLabel
            
        }
        self.numberLabel?.text = "\(currentIndex) / \(coupons.count)"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func viewControllerAtIndex(index index: Int) -> CouponDetailsViewController
    {
        let childViewController = CouponDetailsViewController.create()
        childViewController.index = index
        childViewController.couponItem = self.coupons[index]
        childViewController.coupondDelegate = self
        return childViewController
    }
    
    
    //MARK: -UIPageViewControllerDataSource
    func pageViewController(pageViewController: UIPageViewController,
        viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
    {
        var index = (viewController as! CouponDetailsViewController).index
        if index == 0
        {
            return nil
        }
        index!--
        return self.viewControllerAtIndex(index: index!)
    }
    
    func pageViewController(pageViewController: UIPageViewController,
        viewControllerAfterViewController viewController: UIViewController) -> UIViewController?
    {
        var index = (viewController as! CouponDetailsViewController).index
        index!++
        if index == coupons.count
        {
            return nil
        }
        return self.viewControllerAtIndex(index: index!)
    }
    
    //MARK: UIPageViewControllerDelegate
    func pageViewController(pageViewController: UIPageViewController,
        didFinishAnimating finished: Bool,
        previousViewControllers: [UIViewController],
        transitionCompleted completed: Bool)
    {
        let currentController = pageViewController.viewControllers![0] as! CouponDetailsViewController
        self.displayCurrentPageNumber(currentIndex: currentController.index! + 1)
    }
    
    //MARK: Used coupon delegate
    func cuponUsedWithId(couponId: String)
    {
        self.coupondDelegate?.cuponUsedWithId(couponId)
    }
}
