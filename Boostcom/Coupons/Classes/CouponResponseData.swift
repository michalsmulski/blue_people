//
//  CouponResponseData.swift
//  Boostcom
//
//  Created by Zmicier Biesau on 29.1.16.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import UIKit
import SwiftyJSON



public class CouponResponseData: NSObject {
    
    
    var activationTime:NSDate = NSDate(timeIntervalSince1970: 0)
    var isAlreadyActivated: Bool = false
    
    public init(couponId: String) {
        super.init()
        if let activationTime = CouponsActivationInfo.sharedInstance.getActivationTime(couponId:couponId) {
            self.activationTime = activationTime
            self.isAlreadyActivated = true
        }
    }
    
    public init(data: NSData) {
        super.init()
        parse(JSON(data: data))
    }
    
    private func parse(json: JSON) {
        if let activationTime = json["activatedUntil"].string {//looks like that it is not untill what time activated, looks like that it is time of coupon activation
            self.activationTime = NSDate(string: activationTime, format: .yMdFormat)
        }
        if let errorString = json["errorType"].string {
            if errorString == CouponsConst.ResponseData.kActivatedOfferMessage
            {
                self.isAlreadyActivated = true
            }
        }
        
    }
    
    func connectActivationTimeWithId(couponId: String)
    {
        if !self.isAlreadyActivated
        {
            CouponsActivationInfo.sharedInstance.setActivationTime(time: self.activationTime, couponId: couponId)
        }
        else if let activeteEndTime = CouponsActivationInfo.sharedInstance.getActivationTime(couponId:couponId) {
                self.activationTime = activeteEndTime
            }
        
    }
}
