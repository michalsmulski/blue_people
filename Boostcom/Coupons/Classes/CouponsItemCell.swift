//
//  CouponsItemCell.swift
//  Boostcom
//
//  Created by Zmicier Biesau on 25.1.16.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import UIKit
import Alamofire
import KVNProgress


class CouponsItemCell: UICollectionViewCell {

    @IBOutlet weak var usedMarkImageView: UIImageView!
    @IBOutlet weak var titleImage: UIImageView!
    var isUsed = false
    var couponFile: CouponFile?
        {
        didSet
        {
            let imageURL = NSURL(string: (couponFile?.thumbnailUrl)!)
            Alamofire.request(.GET, imageURL!).response() {
                (_, _, data, _) in
                
                let image = UIImage(data: data!)
                self.titleImage.image = self.isUsed ? image?.toGrayscale() : image
            }
        }
        
   }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.titleImage.layer.borderWidth = 0.5
        self.titleImage.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).CGColor
        
    }

    func markAsUsed(used: Bool)
    {
        self.isUsed = used
        self.usedMarkImageView.hidden = !used
    }
}
