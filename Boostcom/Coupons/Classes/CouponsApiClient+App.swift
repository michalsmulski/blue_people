//
//  CouponsApiClient+App.swift
//  Boostcom
//
//  Created by Zmicier Biesau on 20.1.16.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import Foundation

let API_KEY = "c062951f32c9dca5ad765e1ccabf4c4d"//"728ba31f31fe4a3b3039581e3bb33675"

extension CouponsAPIClient {
    
    static func instance() -> CouponsAPIClient {
        var number = ""
        var countryCode = ""
        if let phoneNumber = UserSession.sharedSession.phoneNumber {
            number = phoneNumber.number
            countryCode = phoneNumber.countryCode()!
        }
        #if DEBUG
            return CouponsAPIClient(baseURL: NSURL(string: "https://apidev.bstcm.no/offers")!, APIKey: API_KEY, phoneNumber: number, countryCode: countryCode)
        #else
            return CouponsAPIClient(baseURL: NSURL(string: "https://api.bstcm.no/offers")!, APIKey: API_KEY, phoneNumber: number, countryCode: countryCode)
        #endif
    }
}