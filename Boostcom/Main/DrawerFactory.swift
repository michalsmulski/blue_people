import UIKit
import MMDrawerController
import UIColor_Hex

class DrawerFactory {
	
	static func drawerViewController(leftViewController: UIViewController, centerViewController: UIViewController) -> MMDrawerController {
		
		let drawerController = MMDrawerController(centerViewController: centerViewController, leftDrawerViewController: leftViewController)
		drawerController.setDrawerVisualStateBlock { (controller, side, percentVisible) in
			let block = MMDrawerVisualState.parallaxVisualStateBlockWithParallaxFactor(4.0)
			block(controller, side, percentVisible)
		}
		drawerController.animationVelocity = 550.0
		drawerController.openDrawerGestureModeMask = .PanningNavigationBar
		drawerController.closeDrawerGestureModeMask = .All
		drawerController.view.backgroundColor = UIColor(hex: 0xFD630A)
		
		drawerController.shadowOffset = CGSizeMake(1.0, 0.0)
		drawerController.shadowRadius = 2.0
		drawerController.shadowOpacity = 0.5
		
		return drawerController
	}
}