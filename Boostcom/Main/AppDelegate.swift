import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	
	var window: UIWindow?
	private(set) var router: Router!
	
	func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
		setupWindow()
		setupRouter()
		router.presentDrawerController(animated: false)
		router.presentInitialViewController()
		return true
	}
	
	private func setupRouter() {
		if let window = window {
			router = Router(window: window)
		}
	}
	
	private func setupWindow() {
		let frame = UIScreen.mainScreen().bounds
		let window = UIWindow(frame: frame)
		window.backgroundColor = UIColor.whiteColor()
		self.window = window
		self.window!.makeKeyAndVisible()
	}
	
	// MARK: user
	
	func userDidAuthenticate() {
		router.presentDrawerController(animated: true)
		router.presentInitialViewController()
	}
	
	func userDidCancelAuthentication() {
		router.presentDrawerController(animated: true)
		router.presentInitialViewController()
	}
	
	func userDidDeAuthenticate() {
		router.presentLoginView(animated: true)
	}
}