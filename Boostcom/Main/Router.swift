import UIKit
import MMDrawerController

public class Router {
	
	private(set) var window: UIWindow
	
	init(window: UIWindow) {
		self.window = window
	}
	
	public func presentDrawerController(animated animated: Bool) {
		
		let menuViewController = UIStoryboard.menu().menuViewController()
		let centerViewController = UIViewController()
		let navigationController = UINavigationController(rootViewController: centerViewController)
		let drawerController = DrawerFactory.drawerViewController(menuViewController, centerViewController: navigationController)
		drawerController.view.backgroundColor = UIColor.whiteColor()
		
		presentViewController(drawerController, animated: animated)
	}
	
	public func presentInitialViewController() {
		if let menuViewController = menuViewController() {
			menuViewController.selectFirstItem()
		}
	}
	
	private func menuViewController() -> MenuViewController? {
		if let drawerController = window.rootViewController as? MMDrawerController {
			return drawerController.leftDrawerViewController as? MenuViewController
		}
		return nil
	}
	
	private func presentViewController(viewController: UIViewController, animated: Bool) {
		if animated {
			UIView.transitionWithView(window, duration: 0.32, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
				self.window.rootViewController = viewController
			}, completion: nil)
		} else {
			window.rootViewController = viewController
		}
	}
	
	func presentLoginView(animated animated: Bool) {
		let controller = UIStoryboard.identity().loginViewController(IdentityModule())
		let navigationController = UINavigationController(rootViewController: controller)
		navigationController.setNavigationBarHidden(true, animated: false)
		presentViewController(navigationController, animated: animated)
	}
}