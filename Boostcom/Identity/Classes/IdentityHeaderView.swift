//
// Created by Michał Smulski on 28/07/15.
// Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit
import PureLayout

public class IdentityHeaderView: UIView {
	
    public static let preferredHeight: CGFloat = 40.0

	public static func create(name name: String, assets: AbstractIdentityAssets) -> IdentityHeaderView {
		let textLabel = UILabel()
		textLabel.text = name
		textLabel.textColor = assets.identityHeaderViewTextColor
		textLabel.font = assets.lightFont.fontWithSize(16.0)
		textLabel.numberOfLines = 0
		textLabel.sizeToFit()
		
		let headerView = IdentityHeaderView()
		headerView.addSubview(textLabel)
		textLabel.autoPinEdgeToSuperviewEdge(.Top, withInset: 0.0, relation: NSLayoutRelation.GreaterThanOrEqual)
		textLabel.autoPinEdgeToSuperviewEdge(.Bottom, withInset: 5.0)
		textLabel.autoPinEdgeToSuperviewEdge(.Trailing, withInset: 16.0)
		textLabel.autoPinEdgeToSuperviewEdge(.Leading, withInset: 16.0)
		return headerView
    }
}
