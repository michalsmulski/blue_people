//
//  InterestItem.swift
//  Apotek1
//
//  Created by Michał Smulski on 09/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import Foundation

public class InterestItem {
	
	public var name: String
	public var selected: Bool
	public var profileForm: ProfileForm
	
	public init(name: String, selected: Bool = false, profileForm: ProfileForm) {
		self.name = name
		self.selected = selected
		self.profileForm = profileForm
	}
}