//
//  PickersDataSource.swift
//  Eurosko
//
//  Created by Michał Smulski on 18/11/15.
//  Copyright © 2015 Boostcom. All rights reserved.
//

import Foundation

class PickersDataSource: PickersControllerDataSource {
	
	func pickerItems(pickersController: PickersController) -> [String] {
		return Gender.values()
	}
	
	func minimumDate(pickersController: PickersController) -> NSDate {
		return NSDate.distantPast()
	}
	
	func maximumDate(pickersController: PickersController) -> NSDate {
		return NSDate.distantFuture()
	}
}