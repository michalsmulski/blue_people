//
//  UserAnalytics.swift
//  Apotek1
//
//  Created by Michał Smulski on 27/09/15.
//  Copyright © 2015 BoostComMedia. All rights reserved.
//

import Foundation

class UserAnalytics {
	
	let user: User
	
	init(user: User) {
		self.user = user
	}
	
	func analyticsProperties() -> [NSObject: AnyObject] {
		var properties: [NSObject: AnyObject] = [:]
		
		properties["$first_name"] = user.name
		properties["$last_name"] = user.surname
		properties["$email"] = user.email
		if let birthdate = user.birthdate {
			properties["birthday"] = birthdate.toString(.yMdFormat)
		}
		
		if let gender = user.gender {
			properties["gender"] = gender.englishString()
		}
		
		return properties
	}
}