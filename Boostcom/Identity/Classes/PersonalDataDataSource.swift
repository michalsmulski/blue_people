//
//  PersonalInformationRegistrationDataSource.swift
//  Impulse
//
//  Created by Michał Smulski on 06/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit


class PersonalDataItems {
	private(set) var items = ProfileItems()
	
	init(assets: AbstractIdentityAssets) {
		items = ProfileRepository.personalDataItems(assets)
	}
	
	func valid() -> Bool {
		let flatItems = items.flatMap { $0 }
		for item in flatItems {
			if !item.valid() {
				return false
			}
		}
		return true
	}
	
	func validationErrors() -> [NSError] {
		let flatItems = items.flatMap { $0 }
		var errors = [NSError]()
		for item in flatItems {
			errors += item.validationErrors()
		}
		return errors
	}
	
	func size() -> Int {
		return items.count
	}
	
	func size(section: Int) -> Int {
		return items[section].count
	}
	
	func item(atIndexPath indexPath: NSIndexPath) -> ProfileItem {
		return items[indexPath.section][indexPath.row]
	}
	
	func updateItem(atIndexPath indexPath: NSIndexPath, value: AnyObject?) {
		item(atIndexPath: indexPath).value = value
	}
	
	func personalDataItems() -> [ProfileItem] {
		return items[1]
	}
}

class PersonalDataDataSource: NSObject, UITableViewDataSource {
	
	weak var parentViewController: PersonalDataViewController?
	private var items: PersonalDataItems
	private(set) var footers = [String]()
	private(set) var headers = [String]()
	private(set) var assets: AbstractIdentityAssets
	
	init(tableView: UITableView, parentViewController: PersonalDataViewController?, assets: AbstractIdentityAssets) {
		self.assets = assets
		self.items = PersonalDataItems(assets: assets)
		super.init()
		setup(tableView)
		setupItems()
		self.parentViewController = parentViewController
	}
	
	func personalDataItems() -> [ProfileItem] {
		return items.personalDataItems()
	}
	
	private func setupItems() {
		footers = createFooters()
		headers = createHeaders()
	}
	
	func item(atIndexPath indexPath: NSIndexPath) -> ProfileItem {
		return items.item(atIndexPath: indexPath)
	}
	
	func updateItem(atIndexPath indexPath: NSIndexPath, value: AnyObject?) {
		items.updateItem(atIndexPath: indexPath, value: value)
	}
	
	private func setup(tableView: UITableView) {
		tableView.registerNib(UINib(nibName: "TextFieldTableViewCell", bundle: NSBundle(forClass: ProfileViewController.classForCoder())), forCellReuseIdentifier: TextFieldTableViewCell.reuseId)
		tableView.registerNib(UINib(nibName: "TextTableViewCell", bundle: NSBundle(forClass: ProfileViewController.classForCoder())), forCellReuseIdentifier: TextTableViewCell.reuseId)
		tableView.dataSource = self
		tableView.rowHeight = 50.0
	}
	
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.size(section)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return items.size()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let dataItem = item(atIndexPath: indexPath)
		switch dataItem.type {
		case .Text, .SecureText, .EmailText:
			return textFieldTableViewCell(atIndexPath: indexPath, tableView: tableView, dataItem: dataItem)
		default:
//		case .Picker, .DatePicker, .Logout:
			return textTableViewCell(atIndexPath: indexPath, tableView: tableView, dataItem: dataItem)
		}
    }
	
	func textTableViewCell(atIndexPath indexPath: NSIndexPath, tableView: UITableView, dataItem: ProfileItem) -> TextTableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier(TextTableViewCell.reuseId, forIndexPath: indexPath) as? TextTableViewCell ?? TextTableViewCell.create()
		cell.assets = assets
		cell.configure(dataItem.icon, placeholder: dataItem.nameValue())
		return cell
	}
	
	func textFieldTableViewCell(atIndexPath indexPath: NSIndexPath, tableView: UITableView, dataItem: ProfileItem) -> TextFieldTableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier(TextFieldTableViewCell.reuseId, forIndexPath: indexPath) as? TextFieldTableViewCell ?? TextFieldTableViewCell.create()
		cell.delegate = parentViewController
		cell.configure(dataItem.icon, placeholder: dataItem.name, type: dataItem.type, assets: assets)
		return cell
	}
	
	func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		if section < headers.count {
			return IdentityHeaderView.create(name: headers[section], assets: assets)
		}
		return nil
	}
	
	func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView?  {
		if section < footers.count {
			return IdentityFooterView.create(name: footers[section], assets: assets)
		}
		return nil
	}
	
	func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return IdentityFooterView.preferredHeight
	}
	
	func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return IdentityHeaderView.preferredHeight
	}
	
	func valid() -> Bool {
		return items.valid()
	}
	
	func validationErrors() -> [NSError] {
		return items.validationErrors()
	}
	
	// MARK: data
	
	private func createHeaders() -> [String] {
		return ["sms_code".localized().uppercaseString, "personal_information".localized().uppercaseString]
	}
	
	private func createFooters() -> [String] {
		return ["sms_send_info".localized()]
	}
}
