//
//  User.swift
//  Impulse
//
//  Created by Michał Smulski on 10/08/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import Foundation
import SwiftyJSON


public protocol Reflectable {
	func properties() -> [Any]
}

public extension Reflectable {
	
	func properties() -> [Any] {
		var string = [Any]()
		for children in Mirror(reflecting: self).children {
			string.append(children.value)
		}
		return string
	}
}


public class User: Reflectable {
	
	public var name: String = ""
	public var surname: String = ""
	public var birthdate: NSDate?
	public var gender: Gender?
	public var zipCode: String?
	public var homeAddress: String?
	public var email: String?
	
	public var password: String = ""
	public var interests: [String] = []
	
	public init() {}
	
	public init(data: NSData) {
		parse(JSON(data: data))
	}
	
	private func parse(json: JSON) {
		let properties = json["properties"]
		name = properties["first_name"].stringValue
		surname = properties["last_name"].stringValue
		
		if let birthdate = properties["birthday"].string {
			self.birthdate = NSDate(string: birthdate, format: .yMdFormat)
		}
		
		if let gender = Gender(rawValue: properties["gender"].stringValue) {
			self.gender = gender
		}
		zipCode = properties["zip_code"].string
		email = properties["email"].string
		homeAddress = properties["address"].string
		interests = properties["interests"].arrayValue.map { $0.stringValue }
	}
}
