//
//  PostAuthenticationOperation.swift
//  Impulse
//
//  Created by Michał Smulski on 14/09/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import Foundation

class PostAuthenticationOperation {
	
	private let phoneNumber: String
	private let password: String
	private let behaviours: AbstractIdentityBehaviours
	
	init(phoneNumber: String, password: String, behaviours: AbstractIdentityBehaviours) {
		self.behaviours = behaviours
		self.phoneNumber = phoneNumber
		self.password = password
	}
	
	func execute() {
		createSession()
		behaviours.userDidAuthenticate()
	}
	
	private func createSession() {
		UserSession.sharedSession.open(phoneNumber: phoneNumber, password: password)
	}
}