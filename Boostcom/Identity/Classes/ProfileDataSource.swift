//
//  PersonalInformationRegistrationDataSource.swift
//  Impulse
//
//  Created by Michał Smulski on 06/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

extension ProfileDataSource {
	
	func profileDataItems() -> [ProfileItem] {
		return items.profileDataItems()
	}
}

class ProfileDataItems {
	
	private var items: ProfileItems = [[]]
	
	init(assets: AbstractIdentityAssets) {
		items = ProfileRepository.profileItems(assets)
	}
	
	func profileDataItems() -> [ProfileItem] {
		return items[0]
	}
	
	func populateItems(user: User) {
		
		for item in profileDataItems() {
			item.populateUsingProfileForm(user)
		}
	}
	
	func item(atIndexPath indexPath: NSIndexPath) -> ProfileItem {
		return items[indexPath.section][indexPath.row]
	}
	
	func updateItem(atIndexPath indexPath: NSIndexPath, value: AnyObject?) {
		item(atIndexPath: indexPath).value = value
	}
	
	func valid() -> Bool {
		
		let flatItems = items.flatMap { $0 }
		let invalidItems = flatItems.filter { !$0.valid() }
		return invalidItems.count == 0
	}
	
	func validationErrors() -> [NSError] {
		
		let flatItems = items.flatMap { $0 }
		var errors = [NSError]()
		for item in flatItems {
			errors += item.validationErrors()
		}
		return errors
	}
	
	func size() -> Int {
		return items.count
	}
	
	func size(section: Int) -> Int {
		return items[section].count
	}
	
}

class ProfileDataSource: NSObject, UITableViewDataSource {
	
	private var items: ProfileDataItems
	private var headers: [String] = ["personal_information".localized().uppercaseString]
	weak var parentViewController: ProfileViewController?
	private var existingUser: User!
	private(set) var assets: AbstractIdentityAssets
	
	init(tableView: UITableView, parentViewController: ProfileViewController?, assets: AbstractIdentityAssets) {
		self.assets = assets
		self.parentViewController = parentViewController
		self.items = ProfileDataItems(assets: assets)
		super.init()
		setupTableView(tableView)	
	}
	
	func populateItems(user: User) {
		items.populateItems(user)
		self.existingUser = user
	}
	
	func user() -> User {
		return ProfileItemUserMapper(personalData: profileDataItems(), existingUser: existingUser).user()
	}
	
	func item(atIndexPath indexPath: NSIndexPath) -> ProfileItem {
		return items.item(atIndexPath: indexPath)
	}
	
	func updateItem(atIndexPath indexPath: NSIndexPath, value: AnyObject?) {
		items.updateItem(atIndexPath: indexPath, value: value)
	}
	
	private func setupTableView(tableView: UITableView) {
		tableView.registerNib(UINib(nibName: "TextFieldTableViewCell", bundle: NSBundle(forClass: ProfileViewController.classForCoder())), forCellReuseIdentifier: TextFieldTableViewCell.reuseId)
		tableView.registerNib(UINib(nibName: "TextTableViewCell", bundle: NSBundle(forClass: ProfileViewController.classForCoder())), forCellReuseIdentifier: TextTableViewCell.reuseId)
		tableView.dataSource = self
		tableView.rowHeight = 50.0
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return items.size(section)
	}
	
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return items.size()
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		
		let dataItem = item(atIndexPath: indexPath)
		switch dataItem.type {
		case .Text, .SecureText, .EmailText:
			return textFieldTableViewCell(atIndexPath: indexPath, tableView: tableView, dataItem: dataItem)
		default:
//		case .Picker, .DatePicker, .Logout:
			return textTableViewCell(atIndexPath: indexPath, tableView: tableView, dataItem: dataItem)
		}
	}
	
	func textTableViewCell(atIndexPath indexPath: NSIndexPath, tableView: UITableView, dataItem: ProfileItem) -> TextTableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier(TextTableViewCell.reuseId, forIndexPath: indexPath) as? TextTableViewCell ?? TextTableViewCell.create()
		cell.assets = assets
		cell.configure(dataItem.icon, value: dataItem.stringValue(), placeholder: dataItem.name)
		return cell
	}
	
	func textFieldTableViewCell(atIndexPath indexPath: NSIndexPath, tableView: UITableView, dataItem: ProfileItem) -> TextFieldTableViewCell {
		
		let cell = tableView.dequeueReusableCellWithIdentifier(TextFieldTableViewCell.reuseId, forIndexPath: indexPath) as? TextFieldTableViewCell ?? TextFieldTableViewCell.create()
		cell.delegate = parentViewController
		cell.configure(dataItem.icon, value: dataItem.stringValue(), placeholder: dataItem.name, type: dataItem.type, assets: assets)
		return cell
	}
	
	func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		if section < headers.count {
			return IdentityHeaderView.create(name: headers[section], assets: assets)
		}
		return nil
	}
	
	func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		if section < headers.count {
			return IdentityHeaderView.preferredHeight
		}
		return 0.0
	}
	
	func valid() -> Bool {
		return items.valid()
	}
	
	func validationErrors() -> [NSError] {
		return items.validationErrors()
	}

	
	// MARK: analytics
	
//	private func updateAnalytics(user user: User) {
//		
//		let properties = UserAnalytics(user: user).analyticsProperties()
//		Analytics.sharedInstance.setSuperProperties(properties)
//	}
}
