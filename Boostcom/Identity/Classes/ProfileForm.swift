//
//  ProfileForm.swift
//  AuthenticationSDK
//
//  Created by Michał Smulski on 29/10/15.
//  Copyright © 2015 CocoaPods. All rights reserved.
//

import Foundation

public enum ProfileForm: String {
	case Name = "name"
	case Surname = "surname"
	case Birthdate = "birthdate"
	case Gender = "gender"
	case PostCode = "postcode"
	case SMSCode = "smscode"
	case Email = "email"
	case PhoneNumber = "phoneNumber"
	case HomeAddress = "home_address"
	
	case InterestsGreatDeals = "Gode tilbud"
	case InterestsSkincare = "Hudpleie"
	case InterestsHealth = "Helse og legemidler"
	case InterestsChildren = "Baby/barn"
	case InterestsTravel = "Reise og fritid"
	case InterestsNutrition = "Kost og ernæring"
}