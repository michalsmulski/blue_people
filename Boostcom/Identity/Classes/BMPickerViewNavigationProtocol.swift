//
//  BMPickerViewNavigationProtocol.swift
//  Impulse
//
//  Created by Michał Smulski on 07/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

public protocol BMPickerViewBaseProtocol {
	func pickerViewDidHide(pickerView: UIView)
}
