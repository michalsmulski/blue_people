//
//  NSError+Impulse.swift
//  Impulse
//
//  Created by Michał Smulski on 10/08/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

let SDK_ERROR_DOMAIN = "sdk_error_domain"

enum ErrorCode: Int {
	case Generic = 0
	case Unauthorized
	case InvalidUsername
	case LoginFailed
	case UserConflict
}

extension NSError {
	
	public static func unauthorizedError() -> NSError {
		return NSError(domain: SDK_ERROR_DOMAIN, code: ErrorCode.Unauthorized.rawValue, userInfo: [
			"message" : "Vennligst Logg inn først for å få tilgang til denne delen av søknaden",
			"developer_message" : "Resource access requires user credentials"
			])
	}
	
	static func invalidUsernameError() -> NSError {
		return NSError(domain: SDK_ERROR_DOMAIN, code: ErrorCode.InvalidUsername.rawValue, userInfo: [
			"message" : "Ugyldig mobilnummer",
			"developer_message" : "Wrong msisdn in request"
			])
	}
	
	static func genericError() -> NSError {
		return NSError(domain: SDK_ERROR_DOMAIN, code: ErrorCode.Generic.rawValue, userInfo: [
			"message" : "Det har oppstått en feil. Vil du laste inn på nytt?",
			"developer_message" : "Yeah, something broke in the app. Mind reloading?"
			])
	}
	
	static func loginFailedError() -> NSError {
		return NSError(domain: SDK_ERROR_DOMAIN, code: ErrorCode.LoginFailed.rawValue, userInfo: [
			"message" : "Feil mobilnummer eller SMS kode",
			"developer_message" : "Login failed. Probably wrong sms code or phone number"
			])
	}
	
	static func userConflictError() -> NSError {
		return NSError(domain: SDK_ERROR_DOMAIN, code: ErrorCode.UserConflict.rawValue, userInfo: [
			"message" : "En bruker med dette mobilnummeret eksisterer allerede",
			"developer_message" : "Registering phone number which exists in data base already"
			])
	}
}
