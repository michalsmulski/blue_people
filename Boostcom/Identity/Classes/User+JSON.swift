//
//  User+JSON.swift
//  Eurosko
//
//  Created by Michał Smulski on 17/11/15.
//  Copyright © 2015 Boostcom. All rights reserved.
//

import Foundation

public protocol JSONConvertiable {
	
	func json() -> [String: AnyObject]
}

extension User: JSONConvertiable {
	
	public func json() -> [String: AnyObject] {
		var dict: [String: AnyObject] = [
			"first_name": name,
			"last_name": surname,
		]
		
		if let email = email {
			dict["email"] = email
		}
		
		if let zipCode = zipCode {
			dict["zip_code"] = zipCode
		}
		
		if let birthdate = birthdate {
			dict["birthday"] = birthdate.toString(.yMdDashFormat)
		}
		
		if let gender = gender {
			dict["gender"] = gender.rawValue
		}
		
		if interests.count > 0 {
			dict["interests"] = interests
		}
		
		return [ "properties": dict ]
	}
}