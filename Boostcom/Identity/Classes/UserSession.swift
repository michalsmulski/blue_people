//
//  UserSession.swift
//  Apotek1
//
//  Created by Michał Smulski on 06/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

private let USER_DEFAULT_PHONE_NUMBER_KEY = "phone_number_key"
private let USER_DEFAULT_PASSWORD_KEY = "password_key"
private let USER_DEFAULT_EMAIL_KEY = "email_key"

import Foundation


public class UserSession {
	
	public static let sharedSession = UserSession()
	
	public var username: String? {
		if let phoneNumber = NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT_PHONE_NUMBER_KEY) as? String {
			return PhoneNumber(number: phoneNumber).numberWithCountryCode
		}
		return nil
	}
	
	public var phoneNumber: PhoneNumber? {
		if let phoneNumber = NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT_PHONE_NUMBER_KEY) as? String {
			return PhoneNumber(number: phoneNumber)
		} else {
			return nil
		}
	}
	
	public var email: String? {
		return NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT_EMAIL_KEY) as? String
	}
	
	public var password: String? {
		if let password = NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT_PASSWORD_KEY) as? String {
			return password
		}
		return nil
	}
	
	public func isOpen() -> Bool {
		return NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT_PHONE_NUMBER_KEY) != nil &&
			NSUserDefaults.standardUserDefaults().objectForKey(USER_DEFAULT_PASSWORD_KEY) != nil
	}
	
	public func open(phoneNumber phoneNumber: String, password: String) {
		NSUserDefaults.standardUserDefaults().setObject(phoneNumber, forKey: USER_DEFAULT_PHONE_NUMBER_KEY)
		NSUserDefaults.standardUserDefaults().setObject(password, forKey: USER_DEFAULT_PASSWORD_KEY)
		NSUserDefaults.standardUserDefaults().synchronize()
	}
	
	public func closeActiveSession() {
		NSUserDefaults.standardUserDefaults().removeObjectForKey(USER_DEFAULT_PHONE_NUMBER_KEY)
		NSUserDefaults.standardUserDefaults().removeObjectForKey(USER_DEFAULT_PASSWORD_KEY)
		NSUserDefaults.standardUserDefaults().synchronize()
	}
	
}
