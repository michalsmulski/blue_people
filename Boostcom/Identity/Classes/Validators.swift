//
//  TextValidator.swift
//  Impulse
//
//  Created by Michał Smulski on 10/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import Foundation

public let BMDataValidationErrorDomain = "data_validation_error_domain"

public enum ValidationErrorCode: Int {
	case EmptyText
	case InvalidEmailAddress
	case InvalidDate
	case InvalidPhoneNumber
}

public extension NSError {
	
	public static func emptyTextError(errorMessage: String? = "This field should not be empty") -> NSError {
		return NSError(
			domain: BMDataValidationErrorDomain,
			code: ValidationErrorCode.EmptyText.rawValue,
			userInfo: ["message" : errorMessage!]
		)
	}
	
	public static func invalidEmailError(errorMessage: String? = "Given email is invalid") -> NSError {
		return NSError(
			domain: BMDataValidationErrorDomain,
			code: ValidationErrorCode.InvalidEmailAddress.rawValue,
			userInfo: ["message" : errorMessage!]
		)
	}
	
	public static func invalidDateError(errorMessage: String? = "You must be at least 15 years old to register") -> NSError {
		return NSError(
			domain: BMDataValidationErrorDomain,
			code: ValidationErrorCode.InvalidDate.rawValue,
			userInfo: ["message" : errorMessage!]
		)
	}
	
	public static func invalidPhoneNumberError(errorMessage: String? = "We accept only swedish or norwegian phone numbers") -> NSError {
		return NSError(
			domain: BMDataValidationErrorDomain,
			code: ValidationErrorCode.InvalidPhoneNumber.rawValue,
			userInfo: ["message" : errorMessage!]
		)
	}
}

public class TextValidator {
	
	public static func validate(text: String?, personalizedErrorMessage: String?) -> (Bool, NSError?) {
		
		let error = NSError.emptyTextError(personalizedErrorMessage)
		var isValid = false
		if let someText = text where someText.characters.count > 0 {
			isValid = true
		}
		return (isValid, error)
	}
}

public class EmailValidator {
	static let EMAIL_PATTERN = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
	
	public static func validate(email: String?, personalizedErrorMessage: String?) -> (Bool, NSError?) {
		let error = NSError.invalidEmailError(personalizedErrorMessage)
		var isValid = false
		if let someEmail = email {
			var expression: NSRegularExpression?
			do {
				expression = try NSRegularExpression(pattern: EmailValidator.EMAIL_PATTERN, options: NSRegularExpressionOptions.CaseInsensitive)
			} catch _ {
				isValid = false
			}
			if let expression = expression {
				let range = NSMakeRange(0, someEmail.characters.count)
				let resultRange = expression.rangeOfFirstMatchInString(someEmail, options: NSMatchingOptions.Anchored, range: range)
				isValid = (resultRange.location != NSNotFound)
			}
		}
		return (isValid, error)
	}
}

public class DateValidator {
	
	public static func validate(date: NSDate?, personalizedErrorMessage: String?) -> (Bool, NSError?) {
		let error = NSError.invalidDateError(personalizedErrorMessage)
		var isValid = false
		if let someDate = date {
			let sourceYear = NSCalendar.currentCalendar().components(.Year, fromDate: someDate)
			let thisYear = NSCalendar.currentCalendar().components(.Year, fromDate: NSDate())
			isValid = (thisYear.year - USER_MIN_AGE >= sourceYear.year)
		}
		return (isValid, error)
	}
}