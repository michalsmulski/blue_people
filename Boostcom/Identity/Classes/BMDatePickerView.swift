//
//  BMDatePickerView.swift
//  Impulse
//
//  Created by Michał Smulski on 07/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit
import PureLayout


public protocol BMDatePickerViewDelegate: BMPickerViewBaseProtocol {
	func didSelectDate(date: NSDate)
}

public class BMDatePickerView: UIView {
	
	private(set) var pickerView: UIDatePicker!
	private(set) var toolbar: UIToolbar!
	
	public var delegate: BMDatePickerViewDelegate?
	public var selectedDate: NSDate?
	private var minDate: NSDate?
	private var maxDate: NSDate?
	
	public init(delegate: BMDatePickerViewDelegate? = nil, minDate: NSDate? = nil, maxDate: NSDate? = nil) {
		self.delegate = delegate
		self.minDate = minDate
		self.maxDate = maxDate
		super.init(frame: CGRectMake(0.0, 0.0, 320.0, 194.0))
	}
	
	required public init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	public override func didMoveToSuperview() {
		super.didMoveToSuperview()
		setup()
	}
	
	private func setup() {
		
		let pickerView = UIDatePicker()
		pickerView.minimumDate = minDate
		pickerView.maximumDate = maxDate
		pickerView.datePickerMode = .Date
		
		self.addSubview(pickerView)
		pickerView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsZero, excludingEdge: .Top)
		self.pickerView = pickerView
		
		let toolbar = UIToolbar()
		toolbar.backgroundColor = UIColor.lightGrayColor()
		self.addSubview(toolbar)
		toolbar.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsZero, excludingEdge: .Bottom)
		toolbar.autoPinEdge(.Bottom, toEdge: .Top, ofView: self.pickerView, withOffset: 1.0)
		
		let fixedSpace = UIBarButtonItem(barButtonSystemItem: .FixedSpace, target: nil, action: nil)
		fixedSpace.width = 8.0
		let cancelButton = UIBarButtonItem(title: "cancel".localized(), style: .Plain, target: self, action: "cancelButtonTapped:")
		let actionButton = UIBarButtonItem(title: "Ferdig", style: .Plain, target: self, action: "actionButtonTapped:")
		let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
		toolbar.items = [fixedSpace, cancelButton, space, actionButton, fixedSpace]
		self.toolbar = toolbar
		
		self.backgroundColor = UIColor.whiteColor()
	}
	
	func cancelButtonTapped(sender: UIBarButtonItem) {
		hide()
	}
	
	func actionButtonTapped(sender: UIBarButtonItem) {
		delegate?.didSelectDate(pickerView.date)
		hide()
	}
	
	public func show() {
		self.alpha = 0.0
		let window = UIApplication.sharedApplication().keyWindow!
		window.addSubview(self)
		self.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsZero, excludingEdge: ALEdge.Top)
		self.selectPreviousDate()
		UIView.animateWithDuration(0.32, animations: {
			self.alpha = 1.0
		})
	}
	
	public func selectPreviousDate() {
		if let date = selectedDate {
			pickerView.date = date
		}
	}
	
	public func hide() {
		self.removeFromSuperview()
		delegate?.pickerViewDidHide(self)
	}
}