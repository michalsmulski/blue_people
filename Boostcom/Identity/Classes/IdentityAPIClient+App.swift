//
//  AuthenticationSDK.swift
//  Impulse
//
//  Created by Michał Smulski on 06/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import Foundation

let CUSTOMER_PUBLIC_TOKEN = "308b1550a91d09cc590349de11c48d18"

let API_PRODUCT_NAME = "ios"
let API_SLUG = "falkanger-eurosko"

extension IdentityAPIClient {
	static func instance() -> IdentityAPIClient {
		#if DEBUG
			return IdentityAPIClient(baseURL: NSURL(string: "https://tbp.dev.bstcm.no/api/v1/loyalty_clubs/\(API_SLUG)/members")!, productName: API_PRODUCT_NAME, customerPublicToken: CUSTOMER_PUBLIC_TOKEN)
		#else
			return IdentityAPIClient(baseURL: NSURL(string: "https://tbp.bstcm.no/api/v1/loyalty_clubs/\(API_SLUG)/members")!, productName: API_PRODUCT_NAME, customerPublicToken: CUSTOMER_PUBLIC_TOKEN)
		#endif
	}
}