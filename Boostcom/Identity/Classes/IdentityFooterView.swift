//
//  ImpulseFooterView.swift
//  Impulse
//
//  Created by Michał Smulski on 28/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit
import PureLayout

class IdentityFooterView: UIView {
	
	static let preferredHeight: CGFloat = 30.0
	
	static func create(name name: String, assets: AbstractIdentityAssets) -> IdentityFooterView {
		let textLabel = UILabel()
		textLabel.text = name
		textLabel.textColor = assets.identityFooterViewTextColor
		textLabel.font = assets.lightFont.fontWithSize(13.0)
		textLabel.numberOfLines = 0
		textLabel.sizeToFit()
		
		let footerView = IdentityFooterView()
		footerView.addSubview(textLabel)
		textLabel.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsZero, excludingEdge: .Leading)
		textLabel.autoPinEdgeToSuperviewEdge(.Leading, withInset: 16.0)
		return footerView
	}
}