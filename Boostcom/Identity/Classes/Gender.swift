//
//  Gender.swift
//  Apotek1
//
//  Created by Michał Smulski on 08/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

public enum Gender: String {
	
	case Female = "Kvinne"
	case Male = "Mann"
	
	public static func values() -> [String] {
		
		return [Female, Male].map { $0.rawValue }
	}
	
	public func englishString() -> String {
		
		switch self {
		case .Female: return "female"
		case .Male: return "male"
		}
	}
}
