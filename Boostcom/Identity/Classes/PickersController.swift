//
//  PickersController.swift
//  Impulse
//
//  Created by Michał Smulski on 09/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

@objc public protocol PickersControllerDelegate {
	func datePickerViewDidReturn(date: NSDate)
	func pickerViewDidReturn(value: String)
	
	optional func datePickerViewDidHide()
	optional func pickerViewDidHide()
}

public protocol PickersControllerDataSource {
	func pickerItems(pickersController: PickersController) -> [String]
	
	func minimumDate(pickersController: PickersController) -> NSDate
	func maximumDate(pickersController: PickersController) -> NSDate
}

public class PickersController: BMPickerViewDelegate, BMDatePickerViewDelegate {
	
	private(set) var datePickerView: BMDatePickerView?
	private(set) var pickerView: BMPickerView?
	
	private(set) var pickerDataSource: BMPickerViewDataSource!
	
	public var delegate: PickersControllerDelegate?
	public var dataSource: PickersControllerDataSource?
	
	private(set) var pickerViewPreviousIndex: Int?
	public var datePickerViewPreviousDate: NSDate?
	
	public init(delegate: PickersControllerDelegate? = nil, dataSource: PickersControllerDataSource? = nil) {
		self.delegate = delegate
		self.dataSource = dataSource
	}
	
	public func hideAllPickerViews() {
		erasePickerView(.DatePicker)
		erasePickerView(.Picker)
	}
	
	public func erasePickerView(type: ProfileItemType) {
		if type == .DatePicker {
			datePickerView?.hide()
			datePickerView = nil
		} else if type == .Picker {
			pickerView?.hide()
			pickerView = nil
		}
	}
	
	public func showPickerViewIfPossible(type: ProfileItemType) {
		if type == .DatePicker && datePickerView == nil {
			showPickerView(.DatePicker)
			erasePickerView(.Picker)
		} else if type == .Picker && pickerView == nil {
			showPickerView(.Picker)
			erasePickerView(.DatePicker)
		}
	}
	
	private func updatePickerViewDataSource() {
		var items = [String]()
		if let someDataSource = dataSource {
			items = someDataSource.pickerItems(self)
		}
		pickerDataSource = BMPickerViewDataSource(items: items)
	}
	
	private func showPickerView(type: ProfileItemType) {
		switch type {
		case .DatePicker:
			datePickerView = BMDatePickerView(
				delegate: self,
				minDate: dataSource?.minimumDate(self),
				maxDate: dataSource?.maximumDate(self))
			if let value = datePickerViewPreviousDate {
				datePickerView!.selectedDate = value
			}
//			populateDatePickerView()
			datePickerView!.show()
		case .Picker:
			updatePickerViewDataSource()
			pickerView = BMPickerView(dataSource: pickerDataSource, delegate: self)
			if let index = pickerViewPreviousIndex {
				pickerView!.selectedItemIndex = index
			}
			pickerView!.show()
		default:
			print("does nothing")
		}
	}
	
	public func populateDatePickerView() {
		if let dataSource = dataSource {
			datePickerView!.pickerView.minimumDate = dataSource.minimumDate(self)
			datePickerView!.pickerView.maximumDate = dataSource.maximumDate(self)
		}
	}
	
	public func didSelectDate(date: NSDate) {
		datePickerViewPreviousDate = date
		delegate?.datePickerViewDidReturn(date)
	}
	
	public func didSelectItem(atIndex index: Int) {
		pickerViewPreviousIndex = index
		let name = pickerDataSource.items[index]
		delegate?.pickerViewDidReturn(name)
	}
	
	public func pickerViewDidHide(pickerView: UIView) {
		if datePickerView != nil && pickerView == datePickerView {
			delegate?.datePickerViewDidHide?()
			datePickerView = nil
		} else if self.pickerView != nil && self.pickerView == pickerView {
			self.pickerView = nil
			delegate?.pickerViewDidHide?()
		}
	}
	
}