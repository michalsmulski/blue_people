//
//  AbstractIdentityModule.swift
//  Identity
//
//  Created by Michał Smulski on 30/12/15.
//  Copyright © 2015 CocoaPods. All rights reserved.
//

import UIKit

public protocol AbstractIdentityModule {
	var APIClient: IdentityAPIClient { get }
	var assets: AbstractIdentityAssets { get }
	var behaviours: AbstractIdentityBehaviours { get }
}

public protocol AbstractIdentityAssets {
	var lightFont: UIFont { get }
	var regularFont: UIFont { get }
	var boldFont: UIFont { get }
	
	var textTableViewCellNormalBackgroundColor: UIColor { get }
	var textTableViewCellSelectedBackgroundColor: UIColor { get }
	
	var closeButtonImage: UIImage { get }
	var MSISDNTextFieldBorderColor: UIColor { get }
	var passwordTextFieldBorderColor: UIColor { get }
	var loginViewBackgroundImage: UIImage { get }
	
	var identityFooterViewTextColor: UIColor { get }
	var identityHeaderViewTextColor: UIColor { get }
    
    var identityTintColor: UIColor { get }
    var identityBackgroundColor: UIColor { get }
    var identityAppLogoBackgroundColor: UIColor { get }
    
	var identityCheckmarkImage: UIImage { get }
	var identityCheckmarkHightlightedImage: UIImage { get }
	
	var firstNameImage: UIImage { get }
	var lastNameImage: UIImage { get }
	var logoutImage: UIImage { get }
	var passwordImage: UIImage { get }
	var birthdateImage: UIImage { get }
	var genderImage: UIImage { get }
	var zipCodeImage: UIImage { get }
	var emailImage: UIImage { get }
	var phoneImage: UIImage { get }
    
    var memberCardLogoImage: UIImage { get }
    var birthDateImage: UIImage { get }
    var telephoneImage: UIImage { get }
    
}

public protocol AbstractIdentityBehaviours {
	func userDidDissmisLoginView()
	func userDidLogout()
	func userDidAuthenticate()
}