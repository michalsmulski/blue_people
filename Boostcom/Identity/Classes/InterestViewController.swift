//
//  InterestViewController.swift
//  Eurosko
//
//  Created by Michał Smulski on 24/11/15.
//  Copyright © 2015 Boostcom. All rights reserved.
//

import UIKit


public protocol InterestViewControllerDelegate {
	func interestViewController(controller: InterestViewController, didSelectInterests interests: [String])
}

public class InterestViewController: UIViewController, UITableViewDelegate {
	
	@IBOutlet weak var tableView: UITableView!
	
	private var dataSource: InterestDataSource!
	public var delegate: InterestViewControllerDelegate?
	public var user: User?
	public var module: AbstractIdentityModule!
	
	public override func viewDidLoad() {
		super.viewDidLoad()
		setup()
	}
	
	private func setup() {
		dataSource = InterestDataSource(tableView: tableView, user: user, assets: module.assets)
		tableView.delegate = self
		tableView.rowHeight = CGFloat(CheckmarkTableViewCell.preferredHeight)
		title = "interests".localized().capitalizedString
	}
	
	// MARK: TableView
	
	public func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let selected = dataSource.item(atIndexPath: indexPath).selected
		dataSource.updateItem(atIndexPath: indexPath, selected: !selected)
		
		if let cell = tableView.cellForRowAtIndexPath(indexPath) as? CheckmarkTableViewCell {
			cell.populate(selected: !selected, assets: module.assets)
		}
		
		delegate?.interestViewController(self, didSelectInterests: dataSource.selectedInterestsNames())
	}
	
	public func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		return dataSource.tableView(tableView, viewForHeaderInSection: section)
	}
	
	public func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return dataSource.tableView(tableView, heightForHeaderInSection: section)
	}
}