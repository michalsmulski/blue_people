//
//  UIStoryboard+Authentication.swift
//  Impulse
//
//  Created by Michał Smulski on 06/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

public extension UIStoryboard {
	
	public static func identity() -> UIStoryboard {
		let localBundle = NSBundle(forClass: LoginViewController.classForCoder())
		return UIStoryboard(name: "Identity", bundle: localBundle)
	}
	
	public func loginViewController(module: AbstractIdentityModule) -> LoginViewController {
		let controller = instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
		controller.module = module
		return controller
	}
	
	public func personalDataViewController(module: AbstractIdentityModule) -> PersonalDataViewController {
		let controller = instantiateViewControllerWithIdentifier("PersonalDataViewController") as! PersonalDataViewController
		controller.module = module
		return controller
	}
	
	public func registrationInterestViewController(module: AbstractIdentityModule) -> RegistrationInterestViewController {
		let controller = instantiateViewControllerWithIdentifier("RegistrationInterestViewController") as! RegistrationInterestViewController
		controller.module = module
		return controller
	}
    
    public func memberCardViewController(module: AbstractIdentityModule) -> MemberCardViewController {
        let controller = instantiateViewControllerWithIdentifier("MemberCardViewController") as! MemberCardViewController
        controller.module = module
        return controller
    }
}
