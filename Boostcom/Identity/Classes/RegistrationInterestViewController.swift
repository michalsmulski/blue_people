//
//  InterestsRegistrationViewController.swift
//  Apotek1
//
//  Created by Michał Smulski on 09/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

import KVNProgress

public class RegistrationInterestViewController: UIViewController, InterestViewControllerDelegate {
	
	public var registrationDTO: RegistrationDTO!
	public var module: AbstractIdentityModule!
	private var postAuthenticationOperation: PostAuthenticationOperation!
	private weak var interestViewController: InterestViewController?
	
	public override func viewDidLoad() {
		super.viewDidLoad()
		embedInterestsViewController()
		setup()
	}
	
	private func setup() {
		title = "registration".localized().capitalizedString
		setupRegisterButton()
	}
	
	private func setupRegisterButton() {
		let button = UIBarButtonItem(title: "finish".localized().capitalizedString, style: .Plain, target: self, action: "registerButtonPressed:")
		navigationItem.rightBarButtonItem = button
	}
	
	public func registerButtonPressed(sender: UIBarButtonItem) {
		registerUser(registrationDTO.user!, phoneNumber: registrationDTO.phoneNumber!, password: registrationDTO.password!)
	}
	
	private func registerUser(user: User, phoneNumber: PhoneNumber, password: String) {
		
		KVNProgress.show()
		module.APIClient.registerUser(user.json(), username: phoneNumber.numberWithCountryCode, memberToken: password, completion: { [unowned self] in
			KVNProgress.showSuccess()
			self.executePostAuthenticationOperation(username: phoneNumber.number, password: password)
		}, failure: { [unowned self] error in
			KVNProgress.dismiss()
			ErrorController(parentViewController: self).handle(error, retryBlock: { [unowned self] in
				self.registerUser(user, phoneNumber: phoneNumber, password: password)
			})
		})
	}
	
	private func executePostAuthenticationOperation(username username: String, password: String) {
		postAuthenticationOperation = PostAuthenticationOperation(phoneNumber: username, password: password, behaviours: module.behaviours)
		postAuthenticationOperation!.execute()
	}
	
	private func embedInterestsViewController() {
		if let user = registrationDTO.user {
			let controller = UIStoryboard.interest().interestViewController(module)
			controller.delegate = self
			controller.user = user
			
			controller.willMoveToParentViewController(self)
			self.addChildViewController(controller)
			self.view.addSubview(controller.view)
			controller.didMoveToParentViewController(self)
			controller.view.autoPinEdgesToSuperviewEdges()
			
			interestViewController = controller
		}
	}
	
	public func interestViewController(controller: InterestViewController, didSelectInterests interests: [String]) {
		if let user = registrationDTO.user {
			user.interests = interests
		}
	}
}