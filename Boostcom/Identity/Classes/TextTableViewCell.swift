//
//  TextTableViewCell.swift
//  Impulse
//
//  Created by Michał Smulski on 07/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

class TextTableViewCell: UITableViewCell {
	static let reuseId = "TextTableViewCell"
	
	@IBOutlet var iconImageView: UIImageView!
	@IBOutlet var label: UILabel!
	var assets: AbstractIdentityAssets?
	
	static func create() -> TextTableViewCell {
		return NSBundle.mainBundle().loadNibNamed("TextTableViewCell", owner: self, options: nil)[0] as! TextTableViewCell
	}
	
	func configure(icon: UIImage, value: String? = nil, placeholder: String) {
		iconImageView.image = icon
		
		if let value = value {
			updateTextLabel(value)
		} else {
			updateTextLabel(placeholder)
		}
	}
	
	func updateTextLabel(string: String) {
		label.text = string
	}
	
	override func setSelected(selected: Bool, animated: Bool) {
		bm_setSelected(selected, animated: animated)
	}
	
	override func setHighlighted(highlighted: Bool, animated: Bool) {
		bm_setSelected(highlighted, animated: animated)
	}
	
	private func bm_setSelected(selected: Bool, animated: Bool) {
		let selectionBlock: Void -> Void = {
			if let assets = self.assets {
				if selected {
					self.backgroundColor = assets.textTableViewCellSelectedBackgroundColor
				} else {
					self.backgroundColor = assets.textTableViewCellNormalBackgroundColor
				}
			}
			self.iconImageView.highlighted = selected
		}
		
		if animated {
			UIView.transitionWithView(self.contentView, duration: 0.32, options: .CurveEaseInOut, animations: {
				selectionBlock()
				}, completion: nil)
		} else {
			selectionBlock()
		}
	}
	
}