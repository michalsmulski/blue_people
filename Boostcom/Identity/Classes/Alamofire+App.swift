//
//  Alamofire+Impulse.swift
//  Impulse
//
//  Created by Michał Smulski on 27/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import Foundation
import Alamofire

extension NSMutableURLRequest {
	func updateHTTPHeader(memberToken memberToken: String, productName: String) {
		setValue(memberToken, forHTTPHeaderField: MEMBER_TOKEN_KEY)
		setValue(productName, forHTTPHeaderField: PRODUCT_NAME_KEY)
	}
	
	func updateHTTPHeader(customerPublicToken customerPublicToken: String, productName: String) {
		setValue(customerPublicToken, forHTTPHeaderField: CUSTOMER_PUBLIC_TOKEN_KEY)
		setValue(productName, forHTTPHeaderField: PRODUCT_NAME_KEY)
	}
}

class APIRequest: URLRequestConvertible {
	
	private var method: Alamofire.Method
	private var URL: NSURL
	private var memberToken: String?
	private var parameters: [String: AnyObject]?
	private let productName: String
	private let customerPublicToken: String
	
	init(method: Alamofire.Method, URL: NSURL, memberToken: String? = nil, parameters: [String: AnyObject]? = nil, productName: String, customerPublicToken: String) {
		
		self.method = method
		self.URL = URL
		self.memberToken = memberToken
		self.parameters = parameters
		self.productName = productName
		self.customerPublicToken = customerPublicToken
	}
	
	var URLRequest: NSMutableURLRequest {
		
		let request = NSMutableURLRequest(URL: URL)
		if let token = memberToken {
			request.updateHTTPHeader(memberToken: token, productName: productName)
		} else {
			request.updateHTTPHeader(customerPublicToken: customerPublicToken, productName: productName)
		}
		request.HTTPMethod = method.rawValue
		
		let encoding = ParameterEncoding.URL
		return encoding.encode(request, parameters: parameters).0
	}
}

public extension Manager {
	
	public static func rejectCookiesManager() -> Manager {
		let config = NSURLSessionConfiguration.defaultSessionConfiguration()
		
		let storage = NSHTTPCookieStorage()
		storage.cookieAcceptPolicy = .Never
		config.HTTPCookieStorage = storage
		
		return Manager(configuration: config)
	}
}