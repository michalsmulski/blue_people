//
//  ErrorController.swift
//  Impulse
//
//  Created by Michał Smulski on 10/08/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit


public class ErrorController {
	
	private var retryBlock: (Void -> Void)?
	private(set) var parentViewController: UIViewController
	
	public init(parentViewController: UIViewController) {
		self.parentViewController = parentViewController
	}
	
	private func map() -> [Int: String] {
		return [
			-1009 : "Internett-tilkobling er offline"
		]
	}
	
	private func errorMessage(error: NSError) -> String? {
		
		var message: String? = nil
		
		// check userinfo
		#if DEBUG
			let key = "developer_message"
			#else
			let key = "message"
		#endif
		
		if let errorMessage = error.userInfo[key] as? String {
			message = errorMessage
		} else if let errorMessage = map()[error.code] {
			message = errorMessage
		}
		
		return message
	}

	public func handle(error: NSError, retryBlock: (Void -> Void)? = nil) {
		
		self.retryBlock = retryBlock
		if let message = errorMessage(error) {
			showMessage(message)
		}
	}
	
	private func showMessage(message: String) {
		
		let appName = UIApplication.sharedApplication().applicationName()
		let alertController = UIAlertController(title: appName, message: message, preferredStyle: .Alert)
		alertController.addAction(UIAlertAction(title: "Avbryte", style: .Cancel, handler: nil))
		if let block = retryBlock {
			alertController.addAction(UIAlertAction(title: "Last", style: .Default, handler: { action in
				block()
			}))
		}
		
		parentViewController.presentViewController(alertController, animated: true, completion: nil)
	}
}
