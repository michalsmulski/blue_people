//
//  KeyboardConstraint.swift
//  Eurosko
//
//  Created by Michał Smulski on 25/11/15.
//  Copyright © 2015 Boostcom. All rights reserved.
//

import UIKit

public class KeyboardLayoutConstraint: NSLayoutConstraint {
	
	private var offset: CGFloat = 0
	private var keyboardVisibleHeight: CGFloat = 0
	
	override public func awakeFromNib() {
		super.awakeFromNib()
		
		offset = constant
		addObservers()
	}
	
	deinit {
		removeObservers()
	}
	
	private func addObservers() {
		NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShowNotification:", name: UIKeyboardWillShowNotification, object: nil)
		NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHideNotification:", name: UIKeyboardWillHideNotification, object: nil)
	}
	
	private func removeObservers() {
		NSNotificationCenter.defaultCenter().removeObserver(self)
	}
	
	// MARK: Notification
	
	func keyboardWillShowNotification(notification: NSNotification) {
		guard let userInfo = notification.userInfo else {
			return
		}
		
		saveKeyboardHeight(userInfo)
		updateConstant(keyboardVisible: true)
		layout()
	}
	
	func keyboardWillHideNotification(notification: NSNotification) {
		keyboardVisibleHeight = 0
		updateConstant(keyboardVisible: false)
		layout()
	}
	
	// MARK: tools
	
	private func layout() {
		if let view = secondItem as? UIView {
			UIView.animateWithDuration(0.32, animations: {
				view.layoutIfNeeded()
			})
		}
	}
	
	private func saveKeyboardHeight(userInfo: [NSObject: AnyObject]) {
		if let frameValue = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {
			let newHeight = frameValue.CGRectValue().size.height + 8.0
			keyboardVisibleHeight = newHeight
		}
	}
	
	private func updateConstant(keyboardVisible keyboardVisible: Bool) {
		constant = keyboardVisible ? keyboardVisibleHeight : offset
	}
	
}