//
// Created by Michał Smulski on 08/07/15.
// Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

public typealias ProfileItems = Array<Array<ProfileItem>>

public enum ProfileItemType: Int {
	case Text = 0
	case SecureText = 1
	case EmailText = 2
	case Picker = 3
	case DatePicker = 4
	case Logout = 5
	case Interests = 6
	
	public func requirePicker() -> Bool {
		return self == .DatePicker || self == .Picker
	}
}

public typealias ValidationBlock = AnyObject? -> (Bool, NSError?)

public class ProfileItem: CustomStringConvertible {
	public let icon: UIImage
	public var value: AnyObject?
	public let name: String
	
	public let type: ProfileItemType
	public let profileForm: ProfileForm
	
	public let validationBlock: ValidationBlock?
	public let requiresValidation: Bool
	
	public init(icon: UIImage, name: String, value: AnyObject? = nil, type: ProfileItemType, profileForm: ProfileForm, requiresValidation: Bool = true, validation: ValidationBlock?) {
		self.icon = icon
		self.name = name
		self.value = value
		self.type = type
		self.profileForm = profileForm
		self.validationBlock = validation
		self.requiresValidation = requiresValidation
	}
	
	public func nameValue() -> String {
		return stringValue() ?? name
	}
	
	public func stringValue() -> String? {
		if let value = value as? String {
			return value
		} else if let date = value as? NSDate {
			return date.toString(.yMdFormat)
		}
		return nil
	}
	
	public func valid() -> Bool {
		
		if let value = value, let validationBlock = validationBlock {
			let (validFlag, _) = validationBlock(value)
			return validFlag
		} else if !requiresValidation {
			return true
		}
		return false
	}
	
	public func validationErrors() -> [NSError] {
		var errors: [NSError] = []
		if !valid() {
			if let validationBlock = validationBlock {
				let (validFlag, error) = validationBlock(value)
				if !validFlag, let error = error {
					errors.append(error)
				}
			}
		}
		return errors
	}
	
	public var description: String {
		return stringValue() ?? "No value found"
	}
	
	public func populateUsingProfileForm(user: User) {
		switch profileForm {
		case .Name:
			value = user.name
		case .Surname:
			value = user.surname
		case .Birthdate:
			value = user.birthdate
		case .Gender:
			if let someGender = user.gender {
				value = someGender.rawValue
			}
		case .PostCode:
			value = user.zipCode
		case .HomeAddress:
			value = user.homeAddress
		case .Email:
			value = user.email
		default:
			print("do nothing")
		}
	}
}