//
//  KeyboardController.swift
//  Eurosko
//
//  Created by Michał Smulski on 19/11/15.
//  Copyright © 2015 Boostcom. All rights reserved.
//

import Foundation
import IQKeyboardManager

class KeyboardController {
	
	class var sharedInstance: KeyboardController {
		struct Singleton {
			static let instance = KeyboardController()
		}
		return Singleton.instance
	}
	
	func enable(enable: Bool) {
		IQKeyboardManager.sharedManager().enable = enable
		IQKeyboardManager.sharedManager().enableAutoToolbar = false
		IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
	}
}