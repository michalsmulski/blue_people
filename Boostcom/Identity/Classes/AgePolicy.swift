//
//  AgePolicy.swift
//  Apotek1
//
//  Created by Michał Smulski on 14/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import Foundation

public let USER_MIN_AGE: Int = 15