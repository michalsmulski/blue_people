//
//  TextFieldTableViewCell.swift
//  Impulse
//
//  Created by Michał Smulski on 06/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

public protocol TextFieldTableViewCellDelegate {
	func didBeginEditing(cell: TextFieldTableViewCell)
	func didEndEditing(value: String?, cell: TextFieldTableViewCell)
	func didChangeValue(value: String?, cell: TextFieldTableViewCell)
}

public class TextFieldTableViewCell: UITableViewCell, UITextFieldDelegate {
	public static let reuseId = "TextFieldTableViewCell"
	
	@IBOutlet var iconImageView: UIImageView!
	@IBOutlet var textField: UITextField!
	public var delegate: TextFieldTableViewCellDelegate?
	
	public static func create() -> TextFieldTableViewCell {
		return NSBundle.mainBundle().loadNibNamed("TextFieldTableViewCell", owner: self, options: nil)[0] as! TextFieldTableViewCell
	}
	
	public func configure(icon: UIImage, value: String? = nil, placeholder: String, type: ProfileItemType, assets: AbstractIdentityAssets) {
		iconImageView.image = icon
		
		if let value = value {
			textField.attributedText = NSAttributedString(string: value, attributes: [
				NSFontAttributeName: assets.lightFont.fontWithSize(self.textField.font!.pointSize),
				NSForegroundColorAttributeName: UIColor.blackColor()
				])
		} else {
			textField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [
				NSFontAttributeName: assets.lightFont.fontWithSize(self.textField.font!.pointSize),
				NSForegroundColorAttributeName: UIColor.blackColor()
				])
		}
		textField.accessibilityLabel = String(type.rawValue)
		
		configure(type: type)
	}
	
	public override func awakeFromNib() {
		super.awakeFromNib()
		setup()
	}
	
	private func setup() {
		self.selectionStyle = .None
	}
	
	public func configure(type type: ProfileItemType) {
		textField.delegate = self
		switch type {
		case .Text:
			textField.returnKeyType = UIReturnKeyType.Done
			textField.secureTextEntry = false
			textField.keyboardType = UIKeyboardType.Default
		case .SecureText:
			textField.returnKeyType = UIReturnKeyType.Done
			textField.secureTextEntry = true
			textField.keyboardType = UIKeyboardType.Default
		case .EmailText:
			textField.returnKeyType = UIReturnKeyType.Done
			textField.keyboardType = UIKeyboardType.EmailAddress
			textField.secureTextEntry = false
		default:
			print("does nothing")
		}
	}
	
	// MARK: - TextField Delegate
	
	public func textFieldDidBeginEditing(textField: UITextField) {
		delegate?.didBeginEditing(self)
	}
	
	public func textFieldShouldReturn(textField: UITextField) -> Bool {
		textField.resignFirstResponder()
		delegate?.didEndEditing(textField.text, cell: self)
		return true
	}
	
	public func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
		let finalString = textFieldText(textField.text!, withRange: range, replacementString: string)
		if finalString.characters.count > 0 {
			delegate?.didChangeValue(finalString, cell: self)
		} else {
			delegate?.didChangeValue(nil, cell: self)
		}
		return true
	}
	
	public func textFieldShouldClear(textField: UITextField) -> Bool {
		delegate?.didChangeValue(nil, cell: self)
		return true
	}
	
	private func textFieldText(text: String, withRange range: NSRange, replacementString string: String) -> String {
		if range.length == 1 {
			// delete
			let index = max(0, text.characters.count - 1)
			return (text as NSString).substringToIndex(index)
		} else {
			// add
			return text + string
		}
	}
}
