//
//  UIStoryboard+Interest.swift
//  Apotek1
//
//  Created by Michał Smulski on 03/09/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

extension UIStoryboard {
	
	static func interest() -> UIStoryboard {
		let localBundle = NSBundle(forClass: InterestViewController.classForCoder())
		return UIStoryboard(name: "Interest", bundle: localBundle)
	}
	
	func interestViewController(module: AbstractIdentityModule) -> InterestViewController {
		let controller = instantiateViewControllerWithIdentifier("InterestViewController") as! InterestViewController
		controller.module = module
		return controller
	}
}
