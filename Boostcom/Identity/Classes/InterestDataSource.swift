//
//  InterestRegistrationViewControllerDataSource.swift
//  Apotek1
//
//  Created by Michał Smulski on 09/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

class InterestDataSource: NSObject, UITableViewDataSource {
	
	private(set) var items: [InterestItem] = []
	private(set) var headers: [String] = []
	var user: User?
	var assets: AbstractIdentityAssets
	
	init(tableView: UITableView, user: User? = nil, assets: AbstractIdentityAssets) {
		self.assets = assets
		super.init()
		self.user = user
		items = createItems()
		headers = createHeaders()
		setup(tableView)
		populateItemsFromUser()
	}
	
	func selectedInterestsNames() -> [String] {
		return items.filter { $0.selected }.map { $0.name }
	}
	
	func populateItemsFromUser() {
		if let user = user where user.interests.count > 0 {
			var updatedItems: [InterestItem] = []
			for item in items {
				let filteredInterests = user.interests.filter { $0 == item.name }
				item.selected = filteredInterests.count > 0
				updatedItems.append(item)
			}
			items = updatedItems
		}
	}
	
	func item(atIndexPath indexPath: NSIndexPath) -> InterestItem {
		return items[indexPath.row]
	}
	
	func updateItem(atIndexPath indexPath: NSIndexPath, selected: Bool) {
		let selectedItem = item(atIndexPath: indexPath)
		selectedItem.selected = selected
		items[indexPath.row] = selectedItem
	}
	
	func setup(tableView: UITableView) {
		tableView.dataSource = self
		tableView.registerNib(UINib(nibName: "CheckmarkTableViewCell", bundle: NSBundle(forClass: ProfileViewController.classForCoder())), forCellReuseIdentifier: CheckmarkTableViewCell.reuseId)
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return items.count
	}
	
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier(CheckmarkTableViewCell.reuseId, forIndexPath: indexPath) as? CheckmarkTableViewCell ?? CheckmarkTableViewCell.create()
		configureCell(cell, indexPath: indexPath)
		return cell
	}
	
	func configureCell(cell: CheckmarkTableViewCell, indexPath: NSIndexPath){
		let dataItem = item(atIndexPath: indexPath)
		cell.populate(dataItem.name, selected: dataItem.selected, assets: assets)
	}
	
	func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		if section < headers.count {
			return IdentityHeaderView.create(name: headers[section], assets: assets)
		}
		return nil
	}
	
	func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return IdentityHeaderView.preferredHeight
	}
	
	// MARK: data
	
	func createItems() -> [InterestItem] {
		return [
			InterestItem(name: ProfileForm.InterestsGreatDeals.rawValue, profileForm: ProfileForm.InterestsGreatDeals),
			InterestItem(name: ProfileForm.InterestsChildren.rawValue, profileForm: ProfileForm.InterestsChildren),
			InterestItem(name: ProfileForm.InterestsSkincare.rawValue, profileForm: ProfileForm.InterestsSkincare),
			InterestItem(name: ProfileForm.InterestsHealth.rawValue, profileForm: ProfileForm.InterestsHealth),
			InterestItem(name: ProfileForm.InterestsTravel.rawValue, profileForm: ProfileForm.InterestsTravel),
			InterestItem(name: ProfileForm.InterestsNutrition.rawValue, profileForm: ProfileForm.InterestsNutrition)
		]
	}
	
	func createHeaders() -> [String] {
		return ["select_interests".localized().uppercaseString]
	}
}
