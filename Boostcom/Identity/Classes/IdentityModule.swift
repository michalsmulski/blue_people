//
//  IdentityModule.swift
//  Eurosko
//
//  Created by Michał Smulski on 31/12/15.
//  Copyright © 2015 Boostcom. All rights reserved.
//

import UIKit
import UIColor_Hex

class IdentityModule: AbstractIdentityModule {
	var APIClient = IdentityAPIClient.instance()
	var assets = IdentityAssets() as AbstractIdentityAssets
	var behaviours = IdentityBehaviours() as AbstractIdentityBehaviours
}

class IdentityAssets: AbstractIdentityAssets {
	//TODO: uncomment
//	var lightFont = UIFont(name: "Nunito-Light", size: 1.0)!
//	var regularFont = UIFont(name: "Nunito-Light", size: 1.0)!
//	var boldFont = UIFont(name: "Nunito-Regular", size: 1.0)!
    
    var lightFont = UIFont.systemFontOfSize(1.0)
    var regularFont = UIFont.systemFontOfSize(1.0)
    var boldFont = UIFont.systemFontOfSize(1.0)
	
	var textTableViewCellNormalBackgroundColor = UIColor.whiteColor()
	var textTableViewCellSelectedBackgroundColor = UIColor(hex: 0xefeded) as UIColor
	
	var closeButtonImage = UIImage(named: "login_close")!
	var MSISDNTextFieldBorderColor = UIColor(hex: 0xe0e0e0) as UIColor
	var passwordTextFieldBorderColor = UIColor(hex: 0xe0e0e0)  as UIColor
	var loginViewBackgroundImage = UIImage(named: "login_background_image_large")!
	
	var identityFooterViewTextColor = UIColor.blackColor()
	var identityHeaderViewTextColor = UIColor.blackColor()
	
    var identityTintColor = UIColor(red:0.57, green:0.57, blue:0.57, alpha:1)
    var identityBackgroundColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1)
    var identityAppLogoBackgroundColor = UIColor.whiteColor()
    
	var identityCheckmarkImage = UIImage(named: "profile_checkbox_unchecked")!
	var identityCheckmarkHightlightedImage = UIImage(named: "profile_checkbox_checked")!
	
	var firstNameImage = UIImage(named: "profile_first_name")!
	var lastNameImage = UIImage(named: "profile_last_name")!
	var logoutImage = UIImage(named: "profile_logout")!
	var passwordImage = UIImage(named: "login_sms_code")!
	var birthdateImage = UIImage(named: "profile_birth")!
	var genderImage = UIImage(named: "profile_gender")!
	var zipCodeImage = UIImage(named: "profile_zip")!
	var emailImage = UIImage(named: "profile_email")!
	var phoneImage = UIImage(named: "login_phone")!
    
    var memberCardLogoImage = UIImage(named: "member_card_image")!
    var birthDateImage = UIImage(named: "profile_birth")!
    var telephoneImage = UIImage(named: "login_phone")!
}

class IdentityBehaviours: AbstractIdentityBehaviours {
	
	func userDidDissmisLoginView() {
		let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
		delegate.userDidCancelAuthentication()
	}
	
	func userDidLogout() {
		let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
//		delegate.unregisterUserForPushNotifications()
		delegate.userDidDeAuthenticate()
	}
	
	func userDidAuthenticate() {
		let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
		delegate.userDidAuthenticate()
//		delegate.setupPushNotificationForLoggedUser()
	}
}