//
//  LoginViewController.swift
//  Impulse
//
//  Created by Michał Smulski on 06/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

import PureLayout

private enum AuthenticationProcessState: Int {
	case Unknown = 0
	case UserRecordFound = 1
}

public class LoginViewController: UIViewController, UITextFieldDelegate {
	
	@IBOutlet weak var phoneNumberTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!
	@IBOutlet weak var loginButton: UIButton!
	@IBOutlet weak var closeButton: UIButton!
	@IBOutlet weak var passwordTextFieldHeight: NSLayoutConstraint!
	@IBOutlet weak var backgroundImageView: UIImageView!
	@IBOutlet weak var wheel: UIActivityIndicatorView!
	
	public var module: AbstractIdentityModule!
	private var authenticationState: AuthenticationProcessState = .Unknown
	private var postAuthenticationOperation: PostAuthenticationOperation?
	
	public override func viewDidLoad() {
		super.viewDidLoad()
		setup()
	}
	
	public override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		hideNavigationBarIfAvailable()
		KeyboardController.sharedInstance.enable(true)
	}
	
	public override func viewWillDisappear(animated: Bool) {
		super.viewWillDisappear(animated)
		KeyboardController.sharedInstance.enable(false)
	}
	
	private func hideNavigationBarIfAvailable() {
		if let navigationController = navigationController {
			navigationController.setNavigationBarHidden(true, animated: true)
		}
	}
	
	@IBAction func loginButtonPressed(sender: UIButton) {
		
		if authenticationState == .Unknown {
			processPhoneNumber()
		} else {
			loginUser(PhoneNumber(number: phoneNumberTextField.text!), memberToken: passwordTextField.text!)
		}
	}
	
	@IBAction func closeButtonPressed(sender: UIButton) {
		module.behaviours.userDidDissmisLoginView()
	}
	
	private func processPhoneNumber() {
		if validatePhoneNumber() {
			requestUserRecord(phoneNumberTextField.text!)
		}
	}
	
	private func validatePhoneNumber() -> Bool {
		if let error = PhoneNumber(number: phoneNumberTextField.text!).validate() {
			if let message = error.userInfo["message"] as? String {
				showInvalidPhoneNumberAlert(message)
				return false
			}
		}
		return true
	}
	
	private func showInvalidPhoneNumberAlert(message: String) {
		
		UIView.performWithoutAnimation {
			let alertView = UIAlertView(title: UIApplication.sharedApplication().applicationName(), message: message, delegate: nil, cancelButtonTitle: "cancel".localized())
			alertView.show()
		}
	}
	
	private func setup() {
		
		func authenticationAtrributedString(string: String) -> NSAttributedString {
			let attributedString = NSMutableAttributedString(string: string, attributes: [
				NSForegroundColorAttributeName: UIColor.lightGrayColor(),
				NSFontAttributeName: module.assets.lightFont.fontWithSize(15.0)
				])
			return attributedString
		}
		
		phoneNumberTextField.layer.borderColor = module.assets.MSISDNTextFieldBorderColor.CGColor
		phoneNumberTextField.layer.borderWidth = 1.0
		let phoneImageView = UIImageView(frame: CGRectMake(0, 0, 40.0, 40.0))
		phoneImageView.contentMode = .Center
		phoneImageView.image = module.assets.phoneImage
		phoneNumberTextField.leftView = phoneImageView
		phoneNumberTextField.leftViewMode = .Always
		phoneNumberTextField.attributedPlaceholder = authenticationAtrributedString("phone_number".localized().capitalizedString)
		
		passwordTextField.layer.borderColor = module.assets.passwordTextFieldBorderColor.CGColor
		passwordTextField.layer.borderWidth = 1.0
		let passwordImageView = UIImageView(frame: CGRectMake(0, 0, 40.0, 40.0))
		passwordImageView.contentMode = .Center
		passwordImageView.image = module.assets.passwordImage
		passwordTextField.leftView = passwordImageView
		passwordTextField.leftViewMode = .Always
		passwordTextField.attributedPlaceholder = authenticationAtrributedString("code_from_sms".localized())
		
		closeButton.setImage(module.assets.closeButtonImage, forState: .Normal)
		backgroundImageView.image = module.assets.loginViewBackgroundImage
	}
	
	private func showLoginButton(show: Bool) {
		loginButton.hidden = !show
		if !show {
			wheel.startAnimating()
		} else {
			wheel.stopAnimating()
		}
	}
	
	private func requestUserRecord(number: String) {
		
		showLoginButton(false)
		let phoneNumber = PhoneNumber(number: number)
		let username = phoneNumber.numberWithCountryCode
		module.APIClient.requestUserRecord(username: username, completion: { [unowned self] userRecordFound in
			if userRecordFound {
				self.authenticationState = .UserRecordFound
				self.resendPassword()
				self.showPasswordTextField(true)
			} else {
				self.registerUser()
			}
			self.showLoginButton(true)
		}, failure: { [unowned self] error in
			ErrorController(parentViewController: self).handle(error, retryBlock: { [unowned self] in
				self.requestUserRecord(number)
			})
			self.showLoginButton(true)
		})
	}
	
	private func registerUser() {
		resendPassword()
		presentRegistrationScreen()
	}
	
	private func presentRegistrationScreen() {
		if let navigationController = navigationController {
			let controller = UIStoryboard.identity().personalDataViewController(module)
			let registrationDTO = RegistrationDTO()
			registrationDTO.phoneNumber = PhoneNumber(number: phoneNumberTextField.text!)
			controller.registrationDTO = registrationDTO
			navigationController.setNavigationBarHidden(false, animated: false)
			navigationController.pushViewController(controller, animated: true)
		}
	}
	
	
	private func resendPassword() {
		let username = PhoneNumber(number: phoneNumberTextField.text!).numberWithCountryCode
		module.APIClient.resendPassword(username: username, completion: nil, failure: nil)
	}
	
	private func showPasswordTextField(show: Bool) {
		
		passwordTextFieldHeight.constant = show ? 44.0 : 0.0
		view.setNeedsLayout()
		UIView.animateWithDuration(
			0.64,
			delay: 0.0,
			usingSpringWithDamping: 0.8,
			initialSpringVelocity: 1.0,
			options: UIViewAnimationOptions.CurveEaseInOut,
			animations: {
				self.view.layoutIfNeeded()
			},
			completion: nil)
	}
	
	private func loginUser(phoneNumber: PhoneNumber, memberToken: String) {
		
		showLoginButton(false)
		
		let username = phoneNumber.numberWithCountryCode
		module.APIClient.loginUser(username: username, memberToken: memberToken, completion: { [unowned self] user in
			self.showLoginButton(true)
			self.view.endEditing(true)
			self.executePostAuthenticationOperation()
		}, failure: { [unowned self] error in
			ErrorController(parentViewController: self).handle(error, retryBlock: { [unowned self] in
				self.loginUser(phoneNumber, memberToken: memberToken)
				})
			self.showLoginButton(true)
		})
	}
	
	private func executePostAuthenticationOperation() {
		postAuthenticationOperation = PostAuthenticationOperation(phoneNumber: phoneNumberTextField.text!, password: passwordTextField.text!, behaviours: module.behaviours)
		postAuthenticationOperation!.execute()
	}
	
	private func resetAuthentication() {
		showPasswordTextField(false)
		authenticationState = .Unknown
		passwordTextField.text = nil
	}
	
	// MARK: textfield
	
	public func textFieldShouldClear(textField: UITextField) -> Bool {
		resetAuthentication()
		return true
	}
	
	public func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
		if shouldResetAuthentication(textField) { resetAuthentication() }
		
		return true
	}
	
	private func shouldResetAuthentication(textField: UITextField) -> Bool {
		return textField == phoneNumberTextField && authenticationState != .Unknown
	}
}