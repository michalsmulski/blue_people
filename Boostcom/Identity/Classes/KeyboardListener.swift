//
//  KeyboardListener.swift
//  Eurosko
//
//  Created by Michał Smulski on 18/11/15.
//  Copyright © 2015 Boostcom. All rights reserved.
//

import UIKit

public class KeyboardListener {
	
	private var keyboardWillShowObserver: NSObjectProtocol!
	private var keyboardWillHideObserver: NSObjectProtocol!
	
	public var keyboardWillShowBlock: (NSNotification -> Void)?
	public var keyboardWillHideBlock: (NSNotification -> Void)?
	
	public init() {
		setupObservers()
	}
	
	deinit {
		removeObservers()
	}
	
	private func removeObservers() {
		NSNotificationCenter.defaultCenter().removeObserver(keyboardWillShowObserver)
		NSNotificationCenter.defaultCenter().removeObserver(keyboardWillHideObserver)
		
		keyboardWillShowObserver = nil
		keyboardWillHideObserver = nil
	}
	
	private func setupObservers() {
		keyboardWillShowObserver = NSNotificationCenter.defaultCenter().addObserverForName(UIKeyboardWillShowNotification, object: nil, queue: NSOperationQueue.mainQueue(), usingBlock: { notif in
			if let block = self.keyboardWillShowBlock {
				block(notif)
			}
		})
		
		keyboardWillHideObserver = NSNotificationCenter.defaultCenter().addObserverForName(UIKeyboardWillHideNotification, object: nil, queue: NSOperationQueue.mainQueue(), usingBlock: { notif in
			if let block = self.keyboardWillHideBlock {
				block(notif)
			}
		})
	}
}