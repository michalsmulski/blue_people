//
//  APICommons.swift
//  Apotek1
//
//  Created by Michał Smulski on 08/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import Foundation

let MEMBER_TOKEN_KEY = "X-Member-Token"
let CUSTOMER_PUBLIC_TOKEN_KEY = "X-Customer-Public-Token"
let PRODUCT_NAME_KEY = "X-Product-Name"