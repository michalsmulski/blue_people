//
//  LogoutOperation.swift
//  Impulse
//
//  Created by Michał Smulski on 12/09/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

class LogoutOperation {
	
	private var behaviours: AbstractIdentityBehaviours
	
	init(behaviours: AbstractIdentityBehaviours) {
		self.behaviours = behaviours
	}
	
	func execute() {
		UserSession.sharedSession.closeActiveSession()
		behaviours.userDidLogout()
	}
}
