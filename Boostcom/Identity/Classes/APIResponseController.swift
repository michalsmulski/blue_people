//
//  ResponseController.swift
//  AuthenticationSDK
//
//  Created by Michał Smulski on 29/10/15.
//  Copyright © 2015 CocoaPods. All rights reserved.
//

import Foundation

public class APIResponseController {
	
	public init() {}
	
	public func handle(response: NSHTTPURLResponse?, success: Void -> Void, failure: (NSError, Int) -> Void) {
		if let response = response {
			switch response.statusCode {
			case 200:
				success()
			case 401:
				failure(NSError.unauthorizedError(), response.statusCode)
			default:
				failure(NSError.genericError(), response.statusCode)
			}
		} else {
			failure(NSError.genericError(), 0)
		}
	}
}