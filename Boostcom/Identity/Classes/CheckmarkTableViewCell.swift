//
//  CheckmarkTableViewCell.swift
//  Apotek1
//
//  Created by Michał Smulski on 09/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

public class CheckmarkTableViewCell: UITableViewCell {
	
	public static let reuseId = "CheckmarkTableViewCell"
	public static let preferredHeight = 50.0
	
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var checkmarkImageView: UIImageView!
	
	public static func create() -> CheckmarkTableViewCell {
		return NSBundle.mainBundle().loadNibNamed("CheckmarkTableViewCell", owner: self, options: nil)[0] as! CheckmarkTableViewCell
	}
	
	public func populate(name: String, selected: Bool, assets: AbstractIdentityAssets) {
		nameLabel.text = name
		populate(selected: selected, assets: assets)
	}
	
	public func populate(selected selected: Bool, assets: AbstractIdentityAssets) {
		checkmarkImageView.image = selected ? assets.identityCheckmarkHightlightedImage : assets.identityCheckmarkImage
	}
}