//
//  MemberCardViewController.swift
//  Eurosko
//
//  Created by Axel Zuziak on 05.01.2016.
//  Copyright © 2016 Boostcom. All rights reserved.
//

import UIKit

import KVNProgress

public class MemberCardViewController: UIViewController {
    
    @IBOutlet weak var birthDateLabel: UILabel!
    @IBOutlet weak var birthDateImageView: UIImageView!
    
    @IBOutlet weak var telephoneImageView: UIImageView!
    @IBOutlet weak var telephoneLabel: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!

    @IBOutlet weak var separatorView: UIView!
    
    public var module: AbstractIdentityModule!
    
    private var user: User! {
        didSet {
            fillUserData()
        }
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        loadUserData()
        setup()
    }
    
    private func setup() {
        title = "member_card".localized().uppercaseString
        layoutUI()
    }

    private func layoutUI() {
        view.backgroundColor = module.assets.identityBackgroundColor

        backgroundImageView.backgroundColor = module.assets.identityAppLogoBackgroundColor
        
        logoImageView.image = module.assets.memberCardLogoImage
        birthDateImageView.image = module.assets.birthDateImage.imageWithRenderingMode(.AlwaysTemplate)
        telephoneImageView.image = module.assets.telephoneImage.imageWithRenderingMode(.AlwaysTemplate)
        
        titleLabel.font = module.assets.boldFont.fontWithSize(17.0)
        nameLabel.font = module.assets.regularFont.fontWithSize(25.0)
        birthDateLabel.font = module.assets.regularFont.fontWithSize(13.0)
        telephoneLabel.font = module.assets.regularFont.fontWithSize(13.0)
        
        birthDateLabel.textColor = module.assets.identityTintColor
        telephoneLabel.textColor = module.assets.identityTintColor
        
        birthDateImageView.tintColor = module.assets.identityTintColor
        telephoneImageView.tintColor = module.assets.identityTintColor
        
        separatorView.backgroundColor = module.assets.identityTintColor.colorWithAlphaComponent(0.7)
    }
    
//MARK: User
    
    private func loadUserData() {
        let session = UserSession.sharedSession
        guard session.isOpen() else {
            return
        }
        
        KVNProgress.show()
        module.APIClient.requestUserProfile(username: session.username!, memberToken: session.password!, completion: { [unowned self] user in
                self.user = user
                KVNProgress.dismiss()
            }, failure: { [unowned self] error in
                KVNProgress.dismiss()
                ErrorController(parentViewController: self).handle(error, retryBlock: {
                    
                })
        })
    }
    
    
    private func fillUserData() {
        let session = UserSession.sharedSession
        
        telephoneLabel.text = session.phoneNumber?.numberWithCountryCode
        
        titleLabel.text = "member_card".localized().capitalizedString
        nameLabel.text = user.name + " " + user.surname
        birthDateLabel.text = user.birthdate == nil ? "" : user.birthdate?.toString(.dMyFormat)
    }
}
