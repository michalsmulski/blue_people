//
//  BMDataPickerView.swift
//  Impulse
//
//  Created by Michał Smulski on 07/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import Foundation
import PureLayout

public protocol BMPickerViewDelegate: BMPickerViewBaseProtocol {
	func didSelectItem(atIndex itemIndex: Int)
}

public class BMPickerView: UIView {
	
	private(set) var toolbar: UIToolbar!
	private(set) var pickerView: UIPickerView!
	
	private(set) var delegate: BMPickerViewDelegate?
	public var selectedItemIndex: Int?
	private(set) var dataSource: protocol<UIPickerViewDelegate, UIPickerViewDataSource>?
	
	public init(dataSource: protocol<UIPickerViewDelegate, UIPickerViewDataSource>, delegate: BMPickerViewDelegate? = nil) {
		self.dataSource = dataSource
		self.delegate = delegate
		super.init(frame: CGRectMake(0.0, 0.0, 320.0, 194.0))
	}

	required public init?(coder aDecoder: NSCoder) {
	    super.init(coder: aDecoder)
	}
	
	public override func didMoveToSuperview() {
		super.didMoveToSuperview()
		setup()
	}
	
	private func setup() {
		
		let pickerView = UIPickerView()
		pickerView.dataSource = self.dataSource
		pickerView.delegate = self.dataSource
		
		self.addSubview(pickerView)
		pickerView.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsZero, excludingEdge: .Top)
		self.pickerView = pickerView
		
		let toolbar = UIToolbar()
		toolbar.backgroundColor = UIColor.lightGrayColor()
		self.addSubview(toolbar)
		toolbar.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsZero, excludingEdge: .Bottom)
		toolbar.autoPinEdge(.Bottom, toEdge: .Top, ofView: self.pickerView, withOffset: 1.0)
		
		let fixedSpace = UIBarButtonItem(barButtonSystemItem: .FixedSpace, target: nil, action: nil)
		fixedSpace.width = 8.0
		let cancelButton = UIBarButtonItem(title: "Avbryte", style: .Plain, target: self, action: "cancelButtonTapped:")
		let actionButton = UIBarButtonItem(title: "Ferdig", style: .Plain, target: self, action: "actionButtonTapped:")
		let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
		toolbar.items = [fixedSpace, cancelButton, space, actionButton, fixedSpace]
		self.toolbar = toolbar
		
		self.backgroundColor = UIColor.whiteColor()
	}
	
	public func cancelButtonTapped(sender: UIBarButtonItem) {
		hide()
	}
	
	public func actionButtonTapped(sender: UIBarButtonItem) {
		delegate?.didSelectItem(atIndex: pickerView.selectedRowInComponent(0))
		hide()
	}
	
	public func show() {
		self.alpha = 0.0
		let window = UIApplication.sharedApplication().keyWindow!
		window.addSubview(self)
		self.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsetsZero, excludingEdge: .Top)
		self.selectPreviousItem()
		
		UIView.animateWithDuration(0.32, animations: {
			self.alpha = 1.0
		})
	}
	
	public func selectPreviousItem() {
		if let index = selectedItemIndex {
			pickerView.selectRow(index, inComponent: 0, animated: false)
		}
	}
	
	public func hide() {
		self.removeFromSuperview()
		delegate?.pickerViewDidHide(self)
	}
}
