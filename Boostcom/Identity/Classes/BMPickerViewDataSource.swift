//
//  BMDataPickerViewDataSource.swift
//  Impulse
//
//  Created by Michał Smulski on 07/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

class BMPickerViewDataSource: NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
	
	private(set) var items = [String]()
	
	init(items: [String]) {
		self.items = items
		super.init()
	}
	
	func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		return items.count
	}
	
	func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
		return 1
	}	
	
	func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		return items[row]
	}
	
	func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
		return 30.0
	}
}
