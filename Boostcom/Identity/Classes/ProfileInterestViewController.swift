//
//  ProfileInterestViewController.swift
//  Apotek1
//
//  Created by Michał Smulski on 03/09/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit
import KVNProgress

public class ProfileInterestViewController: UIViewController, InterestViewControllerDelegate {
	
	private weak var interestViewController: InterestViewController?
	public var module: AbstractIdentityModule!
	public var user: User?
	
	public override func viewDidLoad() {
		super.viewDidLoad()
		embedInterestsViewController()
		setup()
	}
	
	private func setup() {
		title = "interests".localized().capitalizedString
		setupSaveButton()
	}
	
	private func setupSaveButton() {
		let button = UIBarButtonItem(title: "save".localized().capitalizedString, style: .Plain, target: self, action: "saveButtonTapped:")
		navigationItem.rightBarButtonItem = button
	}
	
	func saveButtonTapped(sender: UIBarButtonItem) {
		updateUserInterests()
	}
	
	private func updateUserInterests() {
		let session = UserSession.sharedSession
		guard session.isOpen(), let user = user else {
			return
		}
		
		KVNProgress.show()
		module.APIClient.updateUser(user.json(), username: session.username!, memberToken: session.password!, completion: {
			KVNProgress.showSuccess()
			self.navigationController?.popViewControllerAnimated(true)
		}, failure: { [unowned self] error in
			KVNProgress.dismiss()
			ErrorController(parentViewController: self).handle(error, retryBlock: { [unowned self] in
				self.updateUserInterests()
			})
		})
	}
	
	private func embedInterestsViewController() {
		if let user = user {
			let controller = UIStoryboard.interest().interestViewController(module)
			controller.delegate = self
			controller.user = user
			
			controller.willMoveToParentViewController(self)
			self.addChildViewController(controller)
			self.view.addSubview(controller.view)
			controller.didMoveToParentViewController(self)
			controller.view.autoPinEdgesToSuperviewEdges()
			
			interestViewController = controller
		}
	}
	
	public func interestViewController(controller: InterestViewController, didSelectInterests interests: [String]) {
		
		if let user = user {
			user.interests = interests
		}
	}
}
