//
//  AuthenticationAlertController.swift
//  Apotek1
//
//  Created by Michał Smulski on 09/09/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

public class AuthenticationAlertController {
	
	public let presentingViewController: UIViewController
	
	public init(presentingViewController: UIViewController) {
		self.presentingViewController = presentingViewController
	}
	
	public func show(completion: Bool -> Void) {
		let controller = UIAlertController(
			title: UIApplication.sharedApplication().applicationName(),
			message: "For å få tilgang til dette må du være innlogget. Ønsker du å logge inn?",
			preferredStyle: .Alert)
		let cancelAction = UIAlertAction(title: "Nei", style: .Cancel, handler: { action in
			completion(false)
		})
		let loginAction = UIAlertAction(title: "Ja", style: .Default, handler: { action in
			completion(true)
		})
		controller.addAction(cancelAction)
		controller.addAction(loginAction)
		
		presentingViewController.presentViewController(controller, animated: true, completion: nil)
	}
}