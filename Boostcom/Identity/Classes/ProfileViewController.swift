//
//  ProfileViewController.swift
//  Impulse
//
//  Created by Michał Smulski on 12/06/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

import KVNProgress

public class ProfileViewController : UIViewController, UITableViewDelegate, TextFieldTableViewCellDelegate, PickersControllerDelegate {
	
	@IBOutlet weak var tableView: UITableView!
	private var dataSource: ProfileDataSource!
	private var pickersController: PickersController!
	public var module: AbstractIdentityModule!
	private let pickersDataSource = PickersDataSource()
	
	public override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		tableView.deselectSelectedRow(animated: true)
	}
	
	public override func viewWillDisappear(animated: Bool) {
		super.viewWillDisappear(animated)
		dismissInputViews()
	}
	
	public override func viewDidLoad() {
		super.viewDidLoad()
		setup()
		requestUserProfile()
	}
	
	private func setup() {
		setupDataSource()
		pickersController = PickersController(delegate: self, dataSource: pickersDataSource)
		title = "profile".localized()
		tableView.accessibilityLabel = "profile_table_view"
	}
	
	private func showErrors() {
		let errors = dataSource.validationErrors()
		if errors.count > 0, let message = errors[0].userInfo["message"] as? String {
			UIAlertView(
				title: UIApplication.sharedApplication().applicationName(),
				message: message,
				delegate: nil, cancelButtonTitle: "cancel".localized().capitalizedString
				).show()
		}
	}
	
	private func setupSaveButton() {
		navigationItem.rightBarButtonItem = UIBarButtonItem(title: "save".localized(), style: .Plain, target: self, action: "saveButtonTapped:")
	}
	
	public func saveButtonTapped(sender: UIBarButtonItem) {
		if dataSource.valid() {
			dismissInputViews()
			updateUserProfile()
		} else {
			showErrors()
		}
	}
	
	private func dismissInputViews() {
		view.endEditing(true)
		pickersController.hideAllPickerViews()
	}
	
	private func setupDataSource() {
		dataSource = ProfileDataSource(tableView: tableView, parentViewController: self, assets: module.assets)
	}
	
	// MARK: network
	
	private func requestUserProfile() {
		let session = UserSession.sharedSession
		guard session.isOpen() else {
			return
		}
		
		KVNProgress.show()
		tableView.hidden = true
		module.APIClient.requestUserProfile(username: session.username!, memberToken: session.password!, completion: { [unowned self] user in
			KVNProgress.dismiss()
			self.tableView.hidden = false
			self.setupSaveButton()
			self.dataSource.populateItems(user)
			self.tableView.reloadData()
			}, failure: { [unowned self] error in
				KVNProgress.dismiss()
				ErrorController(parentViewController: self).handle(error, retryBlock: { [unowned self] in
					self.requestUserProfile()
					})
			})
	}
	
	
	private func updateUserProfile() {
		let session = UserSession.sharedSession
		guard session.isOpen() else {
			return
		}
		
		KVNProgress.show()
		module.APIClient.updateUser(dataSource.user().json(), username: session.username!, memberToken: session.password!, completion: {
			KVNProgress.showSuccess()
			}, failure: { [unowned self] error in
				KVNProgress.dismiss()
				ErrorController(parentViewController: self).handle(error, retryBlock: { [unowned self] in
					self.updateUserProfile()
					})
			})
	}
	
	// MARK: tableview
	
	public func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		return dataSource.tableView(tableView, viewForHeaderInSection: section)
	}
	
	public func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return dataSource.tableView(tableView, heightForHeaderInSection: section)
	}
	
	public func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let dataItem = dataSource.item(atIndexPath: indexPath)
		tableView.selectedIndexPath = indexPath
		if dataItem.type.requirePicker() {
			self.view.endEditing(true)
			pickersController.showPickerViewIfPossible(dataItem.type)
		} else if dataItem.type == .Logout {
			deselectSelectedRow()
			LogoutOperation(behaviours: module.behaviours).execute()
		} else if dataItem.type == .Interests {
			presentProfileInterestsScreen()
		}
	}
	
	private func presentProfileInterestsScreen() {
		if let navigationController = navigationController {
			let controller = UIStoryboard.profile().profileInterestViewController(module)
			controller.user = dataSource.user()
			navigationController.pushViewController(controller, animated: true)
		}
	}
	
	private func updateDataSource(newValue: AnyObject) {
		if let indexPath = tableView.selectedIndexPath {
			dataSource.updateItem(atIndexPath: indexPath, value: newValue)
		}
	}
	
	private func updateSelectedCell(newName: String) {
		if let indexPath = tableView.selectedIndexPath {
			if let cell = tableView.cellForRowAtIndexPath(indexPath) as? TextTableViewCell {
				cell.updateTextLabel(newName)
			}
		}
	}
	
	// MARK: pickers
	
	public func datePickerViewDidReturn(date: NSDate) {
		updateDataSource(date)
		updateSelectedCell(date.toString(.MdyFormat))
	}
	
	public func pickerViewDidReturn(value: String) {
		updateDataSource(value)
		updateSelectedCell(value)
	}
	
	public func deselectSelectedRow() {
		tableView.deselectSelectedRow(animated: true)
	}
	
	public func pickerViewDidHide() {
		deselectSelectedRow()
		tableView.selectedIndexPath = nil
	}
	
	public func datePickerViewDidHide() {
		deselectSelectedRow()
		tableView.selectedIndexPath = nil
	}
	
	// MARK: - TextFieldTableViewCellDelegate
	
	public func didBeginEditing(cell: TextFieldTableViewCell) {
		pickersController.hideAllPickerViews()
	}
	
	public func didEndEditing(value: String?, cell: TextFieldTableViewCell) {
		if let indexPath = tableView.indexPathForCell(cell) {
			dataSource.updateItem(atIndexPath: indexPath, value: value)
		}
	}
	
	public func didChangeValue(value: String?, cell: TextFieldTableViewCell) {
		if let indexPath = tableView.indexPathForCell(cell) {
			dataSource.updateItem(atIndexPath: indexPath, value: value)
		}
	}
}