//
//  ProfileItemsDataSource.swift
//  Eurosko
//
//  Created by Michał Smulski on 18/11/15.
//  Copyright © 2015 Boostcom. All rights reserved.
//

import UIKit

class ProfileRepository {
	
	static func personalDataItems(assets: AbstractIdentityAssets) -> ProfileItems {
		
		var items: ProfileItems = []
		items.append([
			ProfileItem(icon: assets.passwordImage, name: "code_from_sms".localized(), type: .SecureText, profileForm: .SMSCode, validation: {
				TextValidator.validate($0 as? String, personalizedErrorMessage: "wrong_sms_code".localized())
			})
			])
		items.append(ProfileRepository.items(assets))
		return items
	}
	
	static func profileItems(assets: AbstractIdentityAssets) -> ProfileItems {
		var items: ProfileItems = []
		items.append(ProfileRepository.items(assets))
		items.append([
			ProfileItem(icon: assets.lastNameImage, name: "interests".localized().capitalizedString, type: .Interests, profileForm: .Name, requiresValidation: false, validation: nil)
			])
		items.append([
			ProfileItem(icon: assets.logoutImage, name: "logout".localized().capitalizedString, type: .Logout, profileForm: .Name, requiresValidation: false, validation: nil)
			])
		return items
	}
	
	private static func items(assets: AbstractIdentityAssets) -> [ProfileItem] {
		return [
			ProfileItem(icon: assets.firstNameImage, name: "name".localized(), type: .Text, profileForm: .Name, validation: {
				TextValidator.validate($0 as? String, personalizedErrorMessage: "wrong_name".localized())
			}),
			ProfileItem(icon: assets.lastNameImage, name: "surname".localized(), type: .Text, profileForm: .Surname, validation: {
				TextValidator.validate($0 as? String, personalizedErrorMessage: "wrong_surname".localized())
			}),
			ProfileItem(icon: assets.birthdateImage, name: "birthdate".localized(), type: .DatePicker, profileForm: .Birthdate, requiresValidation: true, validation: {
				DateValidator.validate($0 as? NSDate, personalizedErrorMessage: String.localizedStringWithFormat("must_be_%d_years_old".localized(), USER_MIN_AGE))
			}),
			ProfileItem(icon: assets.genderImage, name: "gender".localized(), type: .Picker, profileForm: .Gender, requiresValidation: false, validation: {
				TextValidator.validate($0 as? String, personalizedErrorMessage: "missing_gender".localized())
			}),
			ProfileItem(icon: assets.zipCodeImage, name: "zip_code".localized(), type: .Text, profileForm: .PostCode, requiresValidation: false, validation: {
				TextValidator.validate($0 as? String, personalizedErrorMessage: "missing_zip_code".localized())
			}),
			ProfileItem(icon: assets.emailImage, name: "email".localized(), type: .EmailText, profileForm: .Email, requiresValidation: false, validation: {
				EmailValidator.validate($0 as? String, personalizedErrorMessage: "wrong_email".localized())
			})
		]
	}
}