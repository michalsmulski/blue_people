//
//  PersonalInfoRegistrationViewController.swift
//  Impulse
//
//  Created by Michał Smulski on 06/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit


public class PersonalDataViewController: UIViewController, UITableViewDelegate, TextFieldTableViewCellDelegate, PickersControllerDelegate {

	@IBOutlet weak var tableView: UITableView!

	private var dataSource: PersonalDataDataSource!
	private var pickersController: PickersController!
	private var pickersDataSource = PickersDataSource()
	public var module: AbstractIdentityModule!

	var registrationDTO: RegistrationDTO!

	public override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		setNavigationBarHiddenIfAvailable(false)
		KeyboardController.sharedInstance.enable(true)
	}

	public override func viewWillDisappear(animated: Bool) {
		super.viewWillDisappear(animated)
		KeyboardController.sharedInstance.enable(false)
	}

	private func setNavigationBarHiddenIfAvailable(hidden: Bool) {
		if let navigationController = navigationController {
			navigationController.setNavigationBarHidden(hidden, animated: true)
		}
	}

	public override func viewDidLoad() {
		super.viewDidLoad()
		setup()
	}

	private func setup() {
		setupDataSource()
		setupInterestsButton()
		pickersController = PickersController(delegate: self, dataSource: pickersDataSource)
		title = "registration".localized().capitalizedString
	}

	private func setupInterestsButton() {
		let button = UIBarButtonItem(title: "interests".localized().capitalizedString, style: .Plain, target: self, action: "interestsButtonTapped:")
		navigationItem.rightBarButtonItem = button
	}

	private func showErrors() {
		let errors = dataSource.validationErrors()
		// TODO: revise
		if let error: NSError? = errors.count > 0 ? errors[0] : nil {
			if let message = error?.userInfo["message"] as? String {
				// TODO: translate dismiss
				UIAlertView(title: UIApplication.sharedApplication().applicationName(),
					message: message,
					delegate: nil, cancelButtonTitle: "Dismiss" ).show()
			}
		}
	}

	func interestsButtonTapped(sender: UIBarButtonItem) {
		if dataSource.valid() {
			updateRegistrationDTO()
			presentInterestsScreen()
		} else {
			showErrors()
		}
	}

	private func updateRegistrationDTO() {
		registrationDTO.user = ProfileItemUserMapper(personalData: dataSource.personalDataItems()).user()
		registrationDTO.password = dataSource.item(atIndexPath: NSIndexPath(forItem: 0, inSection: 0)).stringValue()
	}

	private func presentInterestsScreen() {
		if let navigationController = navigationController {
			let controller = UIStoryboard.identity().registrationInterestViewController(module)
			controller.registrationDTO = registrationDTO
			navigationController.pushViewController(controller, animated: true)
		}
	}

	private func setupDataSource() {
		dataSource = PersonalDataDataSource(tableView: tableView, parentViewController: self, assets: module.assets)
	}

	// MARK: tableview

	public func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
		return dataSource.tableView(tableView, viewForFooterInSection: section)
	}

	public func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		return dataSource.tableView(tableView, viewForHeaderInSection: section)
	}

	public func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return dataSource.tableView(tableView, heightForFooterInSection: section)
	}

	public func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return dataSource.tableView(tableView, heightForHeaderInSection: section)
	}

	public func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let dataItem = dataSource.item(atIndexPath: indexPath)
		if dataItem.type.requirePicker() {
			self.view.endEditing(true)
			pickersController.showPickerViewIfPossible(dataItem.type)
		}
		tableView.selectedIndexPath = indexPath
	}

	func updateDataSource(newValue: AnyObject) {
		if let indexPath = tableView.selectedIndexPath {
			dataSource.updateItem(atIndexPath: indexPath, value: newValue)
		}
	}

	func updateSelectedCell(newName: String) {
		if let indexPath = tableView.selectedIndexPath, let cell = tableView.cellForRowAtIndexPath(indexPath) as? TextTableViewCell {
			cell.updateTextLabel(newName)
		}
	}

	// MARK: pickers

	public func datePickerViewDidReturn(date: NSDate) {
		updateDataSource(date)
		updateSelectedCell(date.toString(.MdyFormat))
	}

	public func pickerViewDidReturn(value: String) {
		updateDataSource(value)
		updateSelectedCell(value)
	}

	public func pickerViewDidHide() {
		tableView.deselectSelectedRow(animated: true)
	}

	public func datePickerViewDidHide() {
		tableView.deselectSelectedRow(animated: true)
	}

	// MARK: - TextFieldTableViewCellDelegate

	public func didBeginEditing(cell: TextFieldTableViewCell) {
		pickersController.hideAllPickerViews()
	}

	public func didEndEditing(value: String?, cell: TextFieldTableViewCell) {
		if let indexPath = tableView.indexPathForCell(cell) {
			dataSource.updateItem(atIndexPath: indexPath, value: value)
		}
	}

	public func didChangeValue(value: String?, cell: TextFieldTableViewCell) {
		if let indexPath = tableView.indexPathForCell(cell) {
			dataSource.updateItem(atIndexPath: indexPath, value: value)
		}
	}
}
