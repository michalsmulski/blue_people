//
//  UIStoryboard+Profile.swift
//  Impulse
//
//  Created by Michał Smulski on 27/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

public extension UIStoryboard {
	
	public static func profile() -> UIStoryboard {
		let localBundle = NSBundle(forClass: ProfileViewController.classForCoder())
		return UIStoryboard(name: "Profile", bundle: localBundle)
	}
	
	public func profileViewController(module: AbstractIdentityModule) -> ProfileViewController {
		let controller = instantiateViewControllerWithIdentifier("ProfileViewController") as! ProfileViewController
		controller.module = module
		return controller
	}
	
	public func profileInterestViewController(module: AbstractIdentityModule) -> ProfileInterestViewController {
		let controller = instantiateViewControllerWithIdentifier("ProfileInterestViewController") as! ProfileInterestViewController
		controller.module = module
		return controller
	}
}
