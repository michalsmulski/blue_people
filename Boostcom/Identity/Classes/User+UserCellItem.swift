//
//  User+UserCellItem.swift
//  Apotek1
//
//  Created by Michał Smulski on 19/08/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

public class ProfileItemUserMapper {
	
	private let personalData: [ProfileItem]
	private var existingUser: User?
	
	public init(personalData: [ProfileItem], existingUser: User? = nil) {
		self.personalData = personalData
		self.existingUser = existingUser
	}
	
	public func user() -> User {
		
		if existingUser == nil {
			existingUser = User()
		}
	
		for item in personalData {
			if let value: AnyObject = item.value {
				setValue(value, withType: item.profileForm, user: existingUser!)
			}
		}
		
		return existingUser!
	}
	
	private func setValue(value: AnyObject, withType type: ProfileForm, user: User) {
		switch type {
		case .Name:
			user.name = value as! String
		case .Surname:
			user.surname = value as! String
		case .Birthdate:
			user.birthdate = value as? NSDate
		case .Gender:
			if let gender = Gender(rawValue: value as! String) {
				user.gender = gender
			}
		case .PostCode:
			user.zipCode = value as? String
		case .Email:
			user.email = value as? String
		case .HomeAddress:
			user.homeAddress = value as? String
		case .SMSCode:
			user.password = value as! String
		default:
			print("do nothing")
		}
	}
}
