//
//  AuthenticationSDK.swift
//  Apotek1
//
//  Created by Michał Smulski on 06/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import Foundation
import Alamofire

public class IdentityAPIClient {
	private let baseURL: NSURL
	private let manager = Manager.rejectCookiesManager()
	private let productName: String
	private let customerPublicToken: String
	
	public init(baseURL: NSURL, productName: String, customerPublicToken: String) {
		self.baseURL = baseURL
		self.productName = productName
		self.customerPublicToken = customerPublicToken
	}
	
	private func baseURLString(username: String) -> String {
		return baseURL.absoluteString + "/" + username
	}
	
	private func extendedBaseURLString(username: String, RESTResourceName: String) -> String {
		return baseURLString(username) + "/" + RESTResourceName
	}
	
	public func requestUserRecord(username username: String, completion: Bool -> Void, failure: NSError -> Void) {
		let URL = NSURL(string: baseURLString(username))!
		let request = APIRequest(method: .HEAD, URL: URL, productName: productName, customerPublicToken: customerPublicToken)
		
		manager.request(request).response { request, response, data, error in
			if let error = error {
				failure(error)
			} else {
				APIResponseController().handle(response, success: {
					completion(true)
				}, failure: { error, statusCode in
					if statusCode == 404 {
						completion(false)
					} else {
						failure(error)
					}
				})
			}
		}
	}
	
	public func resendPassword(username username: String, completion: (Void -> Void)?, failure: (NSError -> Void)?) {
		let URL = NSURL(string: extendedBaseURLString(username, RESTResourceName: "send_token"))!
		let request = APIRequest(method: .POST, URL: URL, productName: productName, customerPublicToken: customerPublicToken)
		
		manager.request(request).response { request, response, data, error in
			if let error = error, failure = failure {
				failure(error)
			} else if let completion = completion {
				completion()
			}
		}
	}
	
	public func loginUser(username username: String, memberToken: String, completion: User -> Void, failure: NSError -> Void) {
		let URL = NSURL(string: baseURLString(username))!
		let request = APIRequest(method: .GET, URL: URL, memberToken: memberToken, productName: productName, customerPublicToken: customerPublicToken)
		
		manager.request(request).response { request, response, data, error in
			if let error = error {
				failure(error)
			} else {
				APIResponseController().handle(response, success: {
					if let data = data {
						completion(User(data: data))
					} else {
						failure(NSError.genericError())
					}
				}, failure: { error, statusCode in
					failure(error)
				})
			}
		}
	}
	
	public func registerUser(json: [String: AnyObject], username: String, memberToken: String, completion: Void -> Void, failure: NSError -> Void) {
		let URL = NSURL(string: baseURLString(username))!
		let request = APIRequest(method: .PUT, URL: URL, memberToken: memberToken, parameters: json, productName: productName, customerPublicToken: customerPublicToken)
		
		manager.request(request).response { request, response, data, error in
			if let error = error {
				failure(error)
			} else {
				APIResponseController().handle(response, success: {
					completion()
				}, failure: { error, statusCode in
					failure(error)
				})
			}
		}
	}
	
	// MARK: User
	
	public func requestUserProfile(username username: String, memberToken: String, completion: User -> Void, failure: NSError -> Void) {
		let URL = NSURL(string: baseURLString(username))!
		let request = APIRequest(method: .GET, URL: URL, memberToken: memberToken, productName: productName, customerPublicToken: customerPublicToken)
		
		manager.request(request).response { request, response, data, error in
			if let error = error {
				failure(error)
			} else {
				APIResponseController().handle(response, success: {
					if let data = data {
						completion(User(data: data))
					}
				}, failure: { error, statusCode in
					failure(error)
				})
			}
		}
	}
	
	public func updateUser(json: [String: AnyObject], username: String, memberToken: String, completion: Void -> Void, failure: NSError -> Void) {
		let URL = NSURL(string: baseURLString(username))!
		let request = APIRequest(method: .PATCH, URL: URL, memberToken: memberToken, parameters: json, productName: productName, customerPublicToken: customerPublicToken)
		
		manager.request(request).response { request, response, data, error in
			if let error = error {
				failure(error)
			} else {
				APIResponseController().handle(response, success: {
					completion()
				}, failure: { error, statusCode in
					failure(error)
				})
			}
		}
	}
	
	public func deleteUser(username: String, password: String) {
		let URL = NSURL(string: baseURLString(username))!
		let request = APIRequest(method: .DELETE, URL: URL, memberToken: password, productName: productName, customerPublicToken: customerPublicToken)
		
		manager.request(request)
	}
}