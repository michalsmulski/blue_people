//
//  RegistrationDTO.swift
//  Eurosko
//
//  Created by Michał Smulski on 16/11/15.
//  Copyright © 2015 Boostcom. All rights reserved.
//

import Foundation


public class RegistrationDTO {
	public var phoneNumber: PhoneNumber?
	public var password: String?
	public var user: User?
}