//
//  String+Size.swift
//  Impulse
//
//  Created by Michał Smulski on 06/10/15.
//  Copyright © 2015 BoostComMedia. All rights reserved.
//

import UIKit

public extension String {
    
	public func size(attributes attrs: [String: AnyObject], constrainedTo box: CGSize) -> CGSize {
		let storage = NSTextStorage(string: self)
		let container = NSTextContainer(size: CGSize(width: box.width, height: box.height))
		let layout = NSLayoutManager()
		layout.addTextContainer(container)
		storage.addLayoutManager(layout)
		storage.addAttributes(attrs, range: NSMakeRange(0, storage.length))
		container.lineFragmentPadding = 0.0
		let _ = layout.glyphRangeForTextContainer(container)
		return layout.usedRectForTextContainer(container).size
	}
}
