//
//  PhoneNumberValidator.swift
//  Apotek1
//
//  Created by Michał Smulski on 06/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import Foundation

let VALIDATION_ERROR_DOMAIN = "validation_error_domain"
let INVALID_PHONE_NUMER_ERROR_CODE = 11

//public extension NSError {
//	
//	static func invalidPhoneNumberError(errorMessage: String? = "Vi godtar bare svenske eller norske telefonnumre") -> NSError {
//		return NSError(
//			domain: VALIDATION_ERROR_DOMAIN,
//			code: INVALID_PHONE_NUMER_ERROR_CODE,
//			userInfo: ["message" : errorMessage!]
//		)
//	}
//}

public class PhoneNumber {
	
	public let number: String
	
	public var numberWithCountryCode: String {
		return countryCode()! + number
	}
	
	public init(number: String) {
		self.number = number
	}
	
	private func mobileNumberMatches(pattern pattern: String) -> Bool {
		
		var regularExpresion: NSRegularExpression?
		do {
			regularExpresion = try NSRegularExpression(pattern: pattern, options: NSRegularExpressionOptions())
		} catch _ {
			regularExpresion = nil
		}
		if let expresion = regularExpresion {
			let range = NSMakeRange(0, number.characters.count)
			let resultRange = expresion.rangeOfFirstMatchInString(number, options: NSMatchingOptions.Anchored, range: range)
			return resultRange.location != NSNotFound
		} else {
			return false
		}
	}
	
	private func isSwedish() -> Bool {
		return mobileNumberMatches(pattern: "^0?7(0|2|3|6)\\d{7}$")
	}
	
	private func isNorwegian() -> Bool {
		return mobileNumberMatches(pattern: "^[49]{1}[0-9]{7}$")
	}
	
	public func validate() -> NSError? {
		if isNorwegian() || isSwedish() {
			return nil
		}
		return NSError.invalidPhoneNumberError()
	}
	
	public func countryCode() -> String? {
		if isNorwegian() { return "47" }
		if isSwedish() { return "46" }
		return nil
	}
}