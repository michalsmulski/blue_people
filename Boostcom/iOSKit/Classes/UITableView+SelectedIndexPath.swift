//
//  UITableView+SelectedIndexPath.swift
//  Apotek1
//
//  Created by Michał Smulski on 20/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import ObjectiveC
import UIKit

public var AssociatedObjectHandle: UInt8 = 0

public extension UITableView {
 
	public var selectedIndexPath: NSIndexPath? {
		get {
			return objc_getAssociatedObject(self, &AssociatedObjectHandle) as? NSIndexPath
		}
		set {
			objc_setAssociatedObject(self, &AssociatedObjectHandle, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
		}
	}
	
	public func deselectSelectedRow(animated animated: Bool) {
		if let indexPath = selectedIndexPath {
			self.deselectRowAtIndexPath(indexPath, animated: animated)
		}
		selectedIndexPath = nil
	}
 
}
