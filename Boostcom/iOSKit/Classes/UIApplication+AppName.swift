//
//  UIApplication+AppName.swift
//  Apotek1
//
//  Created by Michał Smulski on 14/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

public extension UIApplication {
	
	public func applicationName() -> String {
		return NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleExecutable") as! String
	}
 
}
