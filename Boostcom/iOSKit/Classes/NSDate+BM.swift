//
//  NSDate+BCM.swift
//  CouponsApp
//
//  Created by Michał Smulski on 03/06/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import Foundation

private func dateFormatter(format format: String, timeZone: NSTimeZone = NSTimeZone.localTimeZone()) -> NSDateFormatter {
	let formatter = NSDateFormatter()
	formatter.dateFormat = format
	formatter.timeZone = timeZone
	return formatter
}

public enum NSDateFormat: String {
	case yMdDashFormat = "yyyy-MM-dd"
	case yMdFormat = "yyyy/MM/dd"
    case dMyFormat = "dd/MM/yyyy"
	case yMdHmsFormat = "yyyy/MM/dd HH:mm:ss"
	case yMdHmFormat = "yyyy/MM/dd HH:mm"
	case EdMFormat = "EEEE, dd/MM"
	case HmFormat = "HH:mm"
	case dMFormat = "dd/MM"
	case MdyFormat = "MMM dd, yyyy"
	case yMdTHmsZFormat = "yyyy/MM/dd'T'HH:mm:ss'Z'"
}

public extension NSDate {
	
	public func toString(format: NSDateFormat = .yMdHmsFormat) -> String {
		let formatter = NSDateFormatter()
		formatter.dateFormat = format.rawValue
		formatter.timeZone = NSTimeZone.localTimeZone()
		return formatter.stringFromDate(self)
	}
	
	public convenience init(string: String, format: NSDateFormat = .yMdHmsFormat) {
		if let someDate = dateFormatter(format: format.rawValue).dateFromString(string) {
			self.init(timeInterval: 0, sinceDate: someDate)
		} else {
			self.init()
		}
	}
	
	public convenience init(string: String, format: NSDateFormat = .yMdHmsFormat, timeZone: NSTimeZone) {
		if let someDate = dateFormatter(format: format.rawValue, timeZone: timeZone).dateFromString(string) {
			self.init(timeInterval: 0, sinceDate: someDate)
		} else {
			self.init()
		}
	}

}
