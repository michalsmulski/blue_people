//
//  UITableView+HidesSeperator.swift
//  Apotek1
//
//  Created by Michał Smulski on 12/06/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

public extension UITableView {
    
    public func hideSeparatorForEmptyCells() {
        self.tableFooterView = UIView(frame: CGRectZero)
    }
    
    public func showSeparatorForEmptyCells() {
        self.tableFooterView = nil
    }
    
}