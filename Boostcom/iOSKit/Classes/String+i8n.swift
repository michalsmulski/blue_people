//
//  String+i8n.swift
//  Eurosko
//
//  Created by Michał Smulski on 12/11/15.
//  Copyright © 2015 Boostcom. All rights reserved.
//

import Foundation

public extension String {
	
	public func localized(bundle: NSBundle = NSBundle.mainBundle()) -> String {
		return NSLocalizedString(self, tableName: nil, bundle: bundle, value: "", comment: "")
	}
}
