//
//  UINavigationController+UINavigationController_Hooks.swift
//  Impulse
//
//  Created by Michał Smulski on 22/10/15.
//  Copyright © 2015 BoostComMedia. All rights reserved.
//

import UIKit

extension UINavigationController {
	
	func pushViewController(viewController: UIViewController, animated: Bool, completion: Void -> Void) {
		CATransaction.begin()
		CATransaction.setCompletionBlock(completion)
		pushViewController(viewController, animated: animated)
		CATransaction.commit()
	}
}
