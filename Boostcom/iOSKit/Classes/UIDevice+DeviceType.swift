//
//  UIDevice+DeviceType.swift
//  Apotek1
//
//  Created by Michał Smulski on 13/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

public extension UIDevice {
	
	public func newerOrEqualThenIPhone6() -> Bool {
		let screenSize = UIScreen.mainScreen().bounds.size
		let targetedSize = CGSizeMake(375.0, 667.0)
		return screenSize.width >= targetedSize.width &&
				screenSize.height >= targetedSize.height
	}
	
	public func equalToIPhone4And4s() -> Bool {
		let screenSize = UIScreen.mainScreen().bounds.size
		let targetedSize = CGSizeMake(320.0, 480.0)
		return screenSize.width == targetedSize.width &&
			screenSize.height == targetedSize.height
	}
	
}
