//
//  UIImage+Grayscale.swift
//  Impulse
//
//  Created by Michał Smulski on 24/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

public extension UIImage {
	
	public func toGrayscale() -> UIImage {
		let rect = CGRectMake(0.0, 0.0, self.size.width, self.size.height)
		
		let colorSpace = CGColorSpaceCreateDeviceGray()
		let width = CGRectGetWidth(rect)
		let height = CGRectGetHeight(rect)
		
		let context = CGBitmapContextCreate(nil, Int(width), Int(height), 8, 0, colorSpace, CGImageAlphaInfo.None.rawValue)
		CGContextDrawImage(context, rect, self.CGImage)
		let imageRef = CGBitmapContextCreateImage(context)
		
		return UIImage(CGImage: CGImageCreateCopy(imageRef)!)
	}
}
