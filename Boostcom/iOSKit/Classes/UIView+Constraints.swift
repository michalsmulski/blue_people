//
//  UIView+Constraints.swift
//  Impulse
//
//  Created by Michał Smulski on 28/07/15.
//  Copyright (c) 2015 BoostComMedia. All rights reserved.
//

import UIKit

public extension UIView {
	
	public func transcribeConstraints(toView xibView: UIView) {
		let constraints = self.constraints 
		for constraint in constraints
		{
			var firstItem = constraint.firstItem as! NSObject
			if firstItem == self {
				firstItem = xibView
			}
			var secondItem = constraint.secondItem as? NSObject
			if secondItem != nil && secondItem == self {
				secondItem = xibView
			}
			
			xibView.addConstraint(NSLayoutConstraint(
				item: firstItem,
				attribute: constraint.firstAttribute,
				relatedBy: constraint.relation,
				toItem: secondItem,
				attribute: constraint.secondAttribute,
				multiplier: constraint.multiplier,
				constant: constraint.constant)
			)
		}
	}
}
